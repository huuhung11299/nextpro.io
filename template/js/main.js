jQuery(document).ready(function($){
    new WOW().init();
    
    function stickySide(idString, closest, offset){
        if(!$(idString).length) return;
        if(!$(closest).length)  return;
        if(!$(offset))   offset = 0;
        let winTop = $(window).scrollTop();
        let mainHeight = $(closest).height();
        let mainHeightOff = $(closest).offset().top;
        if(winTop + offset >= mainHeightOff &&  winTop + offset + $(idString).height() <= mainHeightOff + mainHeight){
            $(idString).css({
                position : 'relative',
                top : offset+winTop-mainHeightOff+'px'
            });
        } else {
        if(winTop + offset < mainHeightOff){
            $(idString).attr('style','');
        }
            if( winTop + offset + $(idString).height() > mainHeightOff + mainHeight){
                $(idString).css({
                    top: mainHeight - $(idString).height() +'px'
                });
            }
        }
    }

    $('.tab-wrapper').each(function() {
        let $tabWrapper, $tabID;
		$tabWrapper = $(this);
		$tabID = $tabWrapper.find('.tab-link.current').attr('data-tab');
        $tabWrapper.find($tabID).fadeIn().siblings().hide();
        $($tabWrapper).on('click', '.tab-link', function(e){
            e.preventDefault();
			$tabID = $(this).attr('data-tab');
			$(this).addClass('current').siblings().removeClass('current');
			$tabWrapper.find($tabID).fadeIn().siblings().hide();
        });
    });

    $('.main-menu-btn').on('click', function(){
        $(this).addClass('active');
        $('.main-menu').addClass('active');
    });

    $('.main-menu-overlay').on('click', function(){
        $('.main-menu-btn').removeClass('active');
        $('.main-menu').removeClass('active');
    });

    $('.hd-cate-btn').on('click', function(){
        $(this).addClass('active');
        $('.hd-cate').addClass('active');
    });

    $('.hd-cate-overlay').on('click', function(){
        $('.hd-cate-btn').removeClass('active');
        $('.hd-cate').removeClass('active');
    });
    // $(window).on('scroll', function(){
    //     let scrollPos = $(document).scrollTop();
    //     $('.sticky-nav-item a').each(function () {
    //         let currLink = $(this);
    //         let refElement = $(currLink.attr("href"));
    //         if (refElement.position().top <= scrollPos + 150 && refElement.position().top + refElement.height() > scrollPos + 150) {
    //             $('.sticky-nav-item a').closest('li').removeClass("active");
    //             currLink.closest('li').addClass("active");
    //         }
    //         else{
    //             currLink.closest('li').removeClass("active");
    //         }
    //     });
    // });

    if ($('.scroll-top').length) {
		$(window).scroll(function() {
			$(this).scrollTop() > 100 ? $('.scroll-top').addClass('show') : $('.scroll-top').removeClass('show');
		});
		$('.scroll-top').on('click', function(){
			$('html, body').animate({ scrollTop: 0 }, 'slow');
		})
    };
    
    $('.open-popup-btn').magnificPopup({
        removalDelay: 500,
        callbacks: {
            beforeOpen: function() {
                this.st.mainClass = "mfp-zoom-in";
            },
        },
        midClick: true,
        closeBtnInside: false,
    });
    
    $('.open-video-btnm, .open-video-btn-2').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-zoom-in',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false,
    });
    
    $('.faqs-item.active .faqs-content').show();
    $('.faqs-title').on('click', function(){
        $(this).closest('.faqs-item').toggleClass('active').find('.faqs-content').stop().slideToggle();
    });

    $('.main-menu-nav .dropdown > a').append('<i class="fa fa-angle-down" aria-hidden="true"></i>');
    $('.main-menu-nav .dropdown > a > .fa').on('click', function(e){
        e.preventDefault();
        $(this).closest('.dropdown').find('> .sub-menu-wrap').stop().slideToggle();
    });


    if($('.about-direct-slider').length && $('.about-direct-slider-nav').length){
        $('.about-direct-slider').slick({
            dots: false,
            arrows: false,
            prevArrow: '<span class="slick-arrow prev-arrow"></span>',
            nextArrow: '<span class="slick-arrow next-arrow"></span>',
            infinite: true,
            autoplay: false,
            autoplaySpeed: 6000,
            pauseOnFocus: false,
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.about-direct-slider-nav',
        });
    
        $('.about-direct-slider-nav').slick({
            dots: false,
            arrows: true,
            prevArrow: '<span class="slick-arrow prev-arrow"></span>',
            nextArrow: '<span class="slick-arrow next-arrow"></span>',
            infinite: false,
            autoplay: false,
            autoplaySpeed: 6000,
            pauseOnFocus: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 1,
            asNavFor: '.about-direct-slider',
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4,
                        dots: true,
                        arrows: false,
                    }
                }, 
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 3,
                        dots: true,
                        arrows: false,
                    }
                },
                {
                    breakpoint: 501,
                    settings: {
                        slidesToShow: 2,
                        dots: true,
                        arrows: false,
                    }
                },
            ]
        });
    }
    else if($('.about-direct-slider-nav').length){
        $('.about-direct-slider-nav').slick({
            dots: false,
            arrows: true,
            prevArrow: '<span class="slick-arrow prev-arrow"></span>',
            nextArrow: '<span class="slick-arrow next-arrow"></span>',
            infinite: false,
            autoplay: false,
            autoplaySpeed: 6000,
            pauseOnFocus: false,
            speed: 1000,
            slidesToShow: 6,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4,
                        dots: true,
                        arrows: false,
                    }
                }, 
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 3,
                        dots: true,
                        arrows: false,
                    }
                },
                {
                    breakpoint: 501,
                    settings: {
                        slidesToShow: 2,
                        dots: true,
                        arrows: false,
                    }
                },
            ]
        });
    }

    $('.services-slider').slick({
        dots: false,
        arrows: false,
        prevArrow: '<span class="slick-arrow prev-arrow"></span>',
        nextArrow: '<span class="slick-arrow next-arrow"></span>',
        infinite: true,
        autoplay: false,
        autoplaySpeed: 6000,
        pauseOnFocus: false,
        speed: 1000,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                    dots: true,
                    arrows: false,
                }
            }, 
            {
                breakpoint: 501,
                settings: {
                    slidesToShow: 1,
                    dots: true,
                    arrows: false,
                }
            },
        ]
    });

    $('.case-study-slider').slick({
        dots: true,
        arrows: true,
        prevArrow: '<span class="slick-arrow prev-arrow"></span>',
        nextArrow: '<span class="slick-arrow next-arrow"></span>',
        infinite: true,
        autoplay: false,
        autoplaySpeed: 6000,
        pauseOnFocus: false,
        speed: 1000,
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 4,
                    arrows: false,
                }
            }, 
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3,
                    arrows: false,
                }
            },
            {
                breakpoint: 501,
                settings: {
                    slidesToShow: 2,
                    arrows: false,
                }
            },
            {
                breakpoint: 376,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                }
            },
        ]
    });

    $('.w-banner-slider').slick({
        dots: false,
        arrows: false,
        fade:true,
        prevArrow: '<span class="slick-arrow prev-arrow"></span>',
        nextArrow: '<span class="slick-arrow next-arrow"></span>',
        infinite: true,
        autoplay: true,
        autoplaySpeed: 6000,
        pauseOnFocus: false,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        // asNavFor: '.w-banner-slider-nav',
    });

    $('.w-banner-slider-nav').slick({
        dots: false,
        arrows: true,
        prevArrow: '<span class="slick-arrow prev-arrow"></span>',
        nextArrow: '<span class="slick-arrow next-arrow"></span>',
        infinite: true,
        autoplay: true,
        autoplaySpeed: 6000,
        pauseOnFocus: false,
        speed: 1000,
        slidesToShow: 4,
        slidesToScroll: 1,
        // asNavFor: '.w-banner-slider',
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 501,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    });

    $('.term-direct-slider-nav').slick({
        dots: false,
        arrows: false,
        prevArrow: '<span class="slick-arrow prev-arrow"></span>',
        nextArrow: '<span class="slick-arrow next-arrow"></span>',
        infinite: true,
        autoplay: false,
        autoplaySpeed: 6000,
        pauseOnFocus: false,
        speed: 1000,
        slidesToShow: 2,
        slidesToScroll: 2,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 501,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    }).on('afterChange', function(){
        $(this).find('.slick-current').addClass('current').siblings().removeClass('current');
        let $tab = $(this).find('.slick-current').attr('data-tab');
        $($tab).fadeIn().siblings().hide();
    })

    $('.key-insights-slider').slick({
        dots: false,
        arrows: false,
        prevArrow: '<span class="slick-arrow prev-arrow"></span>',
        nextArrow: '<span class="slick-arrow next-arrow"></span>',
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        pauseOnFocus: false,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
    });

    $('.testimonials-slider-wrapper').each(function(){
        let $slider = $(this).find('.testimonials-slider');
        let $sliderAppend = $(this).find('.slider-append');
        $slider.slick({
            dots: false,
            arrows: true,
            prevArrow: '<div class="slick-arrow prev-arrow"></div>',
            nextArrow: '<div class="slick-arrow next-arrow"></div>',
            appendArrows: $sliderAppend,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 6000,
            pauseOnFocus: false,
            speed: 1000,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 501,
                    settings: {
                        slidesToShow: 1,
                    }
                },
            ]
        });
    });
    if($('.home-banner-slider').length && $('.home-banner-slider-nav').length){
        $('.home-banner-slider').slick({
            dots: false,
            arrows: true,
            fade: true,
            prevArrow: '<span class="slick-arrow prev-arrow"></span>',
            nextArrow: '<span class="slick-arrow next-arrow"></span>',
            infinite: true,
            autoplay: true,
            autoplaySpeed: 6000,
            pauseOnFocus: false,
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1,
            // asNavFor: '.home-banner-slider-nav',
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                        arrows: false,
                    }
                },
            ]
        });
        $('.home-banner-slider-nav').slick({
            dots: false,
            arrows: false,
            prevArrow: '<span class="slick-arrow prev-arrow"></span>',
            nextArrow: '<span class="slick-arrow next-arrow"></span>',
            infinite: true,
            autoplay: true,
            autoplaySpeed: 6000,
            pauseOnFocus: false,
            speed: 1000,
            slidesToShow: 5,
            slidesToScroll: 1,
			// rtl: true,
          //  asNavFor: '.home-banner-slider',
            focusOnSelect: true,
            centerMode: true,
            centerPadding: 0,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 501,
                    settings: {
                        slidesToShow: 2,
                    }
                },
            ]
        });
    }

    if($('#main-section').length && $('#sidebar').length){
        let $offset;
        $('.header').length && ($offset = $('.header').outerHeight())
        $('#wpadminbar').length && ($offset = $offset + $('#wpadminbar').outerHeight());
        $(window).on('scroll', function(){
            stickySide('#sidebar', '#main-section', $offset);
        });
    };

    $('.comment-like-reply .reply').on('click', function(e){
        e.preventDefault();
        let $wrapper = $(this).closest('.comment-like-reply');
        if($wrapper.find('#comment-form-reply').length){
            $wrapper.find('#comment-form-reply').stop().toggle();
        }
        else{
            $('#comment-form-reply .f-control').val('');
            $('#comment-form-reply').appendTo($wrapper).show();
        }
    })

    $(window).on('load', function(){
        const $header = $('.header')
        const $headerOffet = $header.offset().top;
        const $headerHeight = $header.outerHeight();
        const $main = $('.main');
        if($(window).scrollTop() > $headerOffet + $headerHeight){
            $header.addClass('fixed');
            $main.css('margin-top', $headerHeight);
        }
        $(window).on('scroll', function(){
            if($(window).scrollTop() > $headerOffet + $headerHeight){
                $header.addClass('fixed');
                $main.css('margin-top', $headerHeight);
            }
            else {
                $header.removeClass('fixed')
                $main.css('margin-top', '');
            }
        });
    });
});