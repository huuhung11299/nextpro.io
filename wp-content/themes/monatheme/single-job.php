<?php
get_header();
while (have_posts()):
    the_post();
    ?>
    <main class="main">
        <div class="position-detail-section sec">
            <div class="container">
                <a href="<?php echo get_permalink( MONA_CAREER ) ?>" class="back-btn">
                    <i class="fa fa-long-arrow-left"></i>
                    <?php echo __('Back','monamedia') ?>
                </a>
                <div class="position-detail-top">
                    <h1 class="position-detail-title"><?php the_title() ?></h1>
                    <div class="position-detail-contact">
                        <p><strong><?php echo __('Location','monamedia') ?>:</strong> <?php echo get_field('location_job_single') ?></p>
                        <p><strong><?php echo __('Job Family','monamedia') ?>:</strong> <?php echo get_field('category_job_single') ?></p>
                        <p><strong><?php echo __('Req ID','monamedia') ?>:</strong> <?php echo get_field('req_id_job') ?></p>
                        
                    </div>
                </div>
                <div class="position-detail-bottom">
                    <div class="position-detail-description">
                        <div class="description-title">
                            <p class="sub-title"><?php echo __('Job Description','monamedia') ?></p>
                            <h2 class="title"><?php echo get_field('sub_title_contact_us_job') ?></h2>
                        </div>
                        <a href="<?php echo get_field('link_title_contact_us_job') ?>" class="main-btn w-auto"><?php echo __('CONTACT US','monamedia') ?></a>
                    </div>
                    <div class="position-detail-content">
                        <div class="mona-content">
                        <?php the_content(  ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php
endwhile;
get_footer();
?>