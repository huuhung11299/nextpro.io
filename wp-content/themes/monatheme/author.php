<?php
get_header();
?>
    <main class="main">
        <div class="banner-section sec" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field('banner_blog_page', MONA_BLOG),'full' ) ?>);">
            <div class="container">
                
                <h1 class="banner-title"><?php echo  get_the_author_meta( 'display_name' ) ?></h1> 
            </div>
        </div>
        <section class="news-section sec">
            <div class="container">
                <div class="news-wrapper">
                    <?php while(have_posts())
                    {
                        the_post();
                        get_template_part( 'patch/post/post', 'item' );
                    }
                    wp_reset_query(  ); ?>
                   
                </div>
                <div class="pagination-wrapper">
                  <?php mona_page_navi(); ?>
                     
                </div>
            </div>
        </section>
    </main>
<?php get_footer();
