<?php
get_header();
while (have_posts()):
    the_post();
    ?>
    <main class="main">
        <div class="sec" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field('image_banner_page_default'),'full') ?>);">
            <div class="container">
                <div class="banner-title"></div> <!-- không xoá -->
                <div class="banner-bottom">
                    <h1 class="banner-title"><?php the_title() ?> </h1> 
                </div>
            </div>
        </div>
        <div class="news-detail-section sec">
            <div class="container">
                <div class="news-detail-content"> 
                    <div class="mona-content training-registration-wrapper">
                        <?php the_content( ) ?>
                    </div>    
                </div>
             
            </div>
        </div>
    </main>
    <?php
endwhile;
get_footer();
?>