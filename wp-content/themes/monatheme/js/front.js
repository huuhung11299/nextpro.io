jQuery(document).ready(function($){
    if($('.main-section').length && $('ul.menu').length){
        let $offset;
        $('.header').length && ($offset = $('.header').outerHeight())
        $('#wpadminbar').length && ($offset = $offset + $('#wpadminbar').outerHeight());
        $(window).on('scroll', function(){
            stickySide('#sidebar', '#main-section', $offset);
        });
    };
    function stickySide(idString, closest, offset){
        if(!$(idString).length) return;
        if(!$(closest).length)  return;
        if(!$(offset))   offset = 0;
        let winTop = $(window).scrollTop();
        let mainHeight = $(closest).height();
        let mainHeightOff = $(closest).offset().top;
        if(winTop + offset >= mainHeightOff &&  winTop + offset + $(idString).height() <= mainHeightOff + mainHeight){
            $(idString).css({
                position : 'relative',
                top : offset+winTop-mainHeightOff+'px'
            });
        } else {
        if(winTop + offset < mainHeightOff){
            $(idString).attr('style','');
        }
            if( winTop + offset + $(idString).height() > mainHeightOff + mainHeight){
                $(idString).css({
                    top: mainHeight - $(idString).height() +'px'
                });
            }
        }
    }; 
    $('.like-comment').on('click', async function (e) { 
        e.preventDefault();
        let $this = $(this);
        let $loading = $this.closest('.mona-like-ajax') ;
        let data = $this.attr('data-id');
        let $action = 'mona_ajax_like_comment';
        if($this.hasClass('active')){
            $action = 'mona_ajax_remove_like'
        }
        try {
            $loading.addClass('loading');
            const result = await sendAjaxPromise(mona_ajax_url.ajaxURL, 'post', {
                action: $action, 
                data: data,
            });
            $loading.removeClass('loading');
            let kq = $.parseJSON(result);
            if(kq.status == 'success'){
                $loading.find('a.like').html( 'Like ' + kq.mess ) ;
                $loading.find('a.like').addClass( 'active' ) ;
            }
            if(kq.status == 'remove'){
                $loading.find('a.like').html( 'Like ' + kq.mess ) ;
                $loading.find('a.like').removeClass( 'active' ) ;
            }
        } catch (error) {
            $loading.removeClass('loading');
            console.log('mona_ajax_call_mfa error:', error);
        }

    });

    $(document).on('click','.mona-ajax-buy', async function(e)
    {
        e.preventDefault();
        let $loading = $('.mona-btn-ajax'), id = $(this).attr('data-id'), $action = 'buy_now', 
        $time =  $("input[name='schedule']:checked"). val(); ;
       
        try {
            $loading.addClass('loading');
            const result = await sendAjaxPromise(mona_ajax_url.ajaxURL, 'post', {
                action: $action, 
                data: id ,
                time: $time
            });
            $loading.removeClass('loading');
            let kq = $.parseJSON(result);
            if(kq.status == 'success')
            {
                // Swal.fire({ 
                //     icon: 'success',
                //     title: 'ENROL THIS COURSE SUCCESS',
                //     showConfirmButton: false,
                //     timer: 1500
                // })
                location.href = kq.mess ; 
            }
            
        } catch (error) 
        {
            $loading.removeClass('loading');
            console.log('mona_ajax_call_mfa error:', error);
        }

    });
    
    
    $('.hung-detail').on('click', async function(e){
        e.preventDefault();
        
        $.magnificPopup.open({
            items: {
              src: '#course-detail-popup'
            }, 
        });
 
        let $this = $(this), id = $this.attr('data-id'), $loading = $('#course-detail-popup'),
        $action = 'mona_load_course' ;

        try {
            
            $loading.addClass('loading');
            const result = await sendAjaxPromise(mona_ajax_url.ajaxURL, 'post', 
                {
                    action: $action, 
                    data: id,
                }
            );
            $loading.removeClass('loading');
            $loading.html(result);
        } catch (error) {
            $loading.removeClass('loading');
            console.log('mona_ajax_call_mfa error:', error);
        } 
       

    }); 
    
    $('.countUp-number').counterUp({
        delay: 10,
        time: 5000
    });

    $(document).on('change','.choose-quantity', async function(){
        let post_data = $( 'form.checkout' ).serialize();

        try { 
            const result = await sendAjaxPromise(mona_ajax_url.ajaxURL, 'post', 
                {
                    action: 'update_order_review', 
                    data: post_data,
                }
            );
        $( 'body' ).trigger( 'update_checkout' );
           
        } catch (error) { 

            console.log('mona_ajax_call_mfa error:', error);
        }  
  
    });

    $(document).on('click','.hung-btn-pay-later',function(e){
        e.preventDefault();
        $('#payment_method_cheque').trigger("click"); 
        $('form.checkout').submit();
    })

    const sendAjaxPromise = (url , type , data) => new Promise((resolve, reject) => { 

        $.ajax({
            url: url,
            type: type,
            data: data,
            success: function(result) {
                resolve(result);
            },
            error: function(error) {
                reject(error);
            }
        });

    });
    

});