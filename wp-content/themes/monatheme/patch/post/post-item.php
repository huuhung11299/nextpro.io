    <div class="news-item">
        <div class="news-img">
            <a href="<?php the_permalink(  ) ?>" class="ratio-box">
                <?php the_post_thumbnail( '470x313' ) ?>
            </a>
        </div>
        <div class="news-info">
            <h4 class="news-title"><a href="<?php the_permalink(  ) ?>"><?php the_title(  ) ?></a></h4> 
            <div class="news-author"><?php the_time( 'M d, Y' ) ?>  <?php echo __('By','monamedia') ?> <a href="<?php echo get_author_posts_url(get_the_author_meta('ID'));; ?>"><strong><?php echo get_the_author_meta( 'nickname' ) ?></strong></a></div>
            <div class="news-desc"><?php the_excerpt(  ) ?></div>
            <a href="<?php the_permalink(  ) ?>" class="readmore-btn"><?php echo __('Read more','monamedia') ?></a>
        </div>
    </div>