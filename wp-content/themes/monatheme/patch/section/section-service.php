    <section class="services-section sec">
        <div class="container">
            <div class="main-title-box wow fadeInLeft">
                <h2 class="main-title"><?php echo get_field('title_section_service') ?></h2>
            </div>
            <div class="services-slider main-slider">
                <?php $service_pages = get_field('items_section_service');
                if( is_array($service_pages) )
                {
                    foreach($service_pages as $key => $item)
                    {
                        ?>
                <div class="slide-item wow <?php echo ($key > 1   ? 'fadeInRight' : 'fadeInLeft')  ?>">
                    <div class="services-item">
                        <h3 class="services-title<?php echo ($item['page_parent'] == get_the_id() ? ' hung-active ' : '') ?>">
                            <span> <?php echo get_the_title($item['page_parent']) ?> </span>
                        </h3>
                        <ul class="services-nav">
                            <?php $page_child = $item['page_children'];
                            if(is_array($page_child))
                            {
                                foreach($page_child as $itm)
                                {
                                    if(get_the_ID() == $itm) $active = 'active'; else $active = '';
                                    echo '<li class="'.$active.'"><a href="'.get_permalink( $itm ).'">'.get_the_title($itm).'</a></li>';
                                }
                            }
                            ?>
                            
                        </ul>
                    </div>
                </div>        
                        <?php
                    }
                }
                ?> 
            </div>
        </div>
    </section>