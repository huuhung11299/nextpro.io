        <?php 
        $content_section = get_field('items_content_management');
        if(is_array($content_section))
        {
            foreach($content_section as $item)
            {
                ?>
        <section class="intro-section sec ">
            <div class="intro-img fadeInLeft wow">
                <?php echo wp_get_attachment_image($item['image'], '1920x400') ?>
            </div>
            <div class="intro-content fadeInRight wow">
                <div class="container">
                    <div class="main-title-box">
                        <h2 class="main-title"><?php echo str_replace('|' , '<br>', $item['title']) ?></h2>
                    </div>
                    <div class="text-wrapper mona-content">
                        <?php echo $item['content']; ?>
                    </div>
                </div>
            </div>
        </section>        
                <?php
            }
        }
        ?>