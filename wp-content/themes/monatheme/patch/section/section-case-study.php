<?php $args = array( 
            'post_type' => 'case',
            'posts_per_page' => 12,
        );
        $case = new WP_Query($args);
        if($case->have_posts())
        {
            ?>
         <section class="case-study-section sec">
            <div class="container">
                <div class="main-title-box fadeInDown wow">
                    <h2 class="main-title"><?php echo __('Case Study','monamedia') ?></h2>
                </div>
            </div>
            <div class="case-study-slider-wrapper">
                <div class="container">
                    <div class="case-study-slider main-slider main-slider-stretch fadeInUp wow">
                        <?php while($case->have_posts())
                        {
                            $case->the_post();
                            get_template_part( 'patch/case/case','item' );

                        }wp_reset_query(  ); ?> 
                    </div>
                </div>
            </div>
        </section>    
            <?php
        }
        ?>
       
       