        <section class="why-choose-us-section sec"> 
            <div class="container">
                <div class="why-choose-us-content wow fadeInLeft">
                    <div class="main-title-box">
                        <h2 class="main-title"><?php echo get_field('title_why_choose_us') ?></h2>
                    </div>
                    <div class="reasons-wrapper">
                        <div class="columns">
                            <?php $reason = get_field('reasons_items_why_choose_us');
                            if( is_array($reason) ) 
                            {
                                foreach($reason as $item) 
                                {
                                    ?>
                            <div class="column">
                                <div class="reasons-item">
                                    <div class="text-wrapper"><?php echo $item['text'] ?></div>
                                </div>
                            </div>        
                                    <?php
                                }
                            }
                            ?>
                            
                        </div>
                    </div>
                </div>
                <div class="why-choose-us-img wow fadeInRight">
                    <div class="ratio-box">
                        <?php  
                        echo wp_get_attachment_image( get_field( 'image_right_why_choose_us' ) , '1360x1360' ) ?>
                    </div>
                </div>
            </div>
        </section>