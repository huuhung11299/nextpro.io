<div class="banner-section sec" style="background-image: url(<?php  echo wp_get_attachment_image_url( get_field('background_banner_management') , '1920x590' ) ?>);">
    <div class="container wow fadeIn">
        <h2 class="banner-title"><?php echo get_field('title_banner_managementIma') ?></h2>
        <h3 class="mb-auto"><?php echo get_field('subtitle_banner_managementIma') ?></h3>
        <div class="banner-sub-title"><?php echo get_field('content_banner_managementIma') ?></div>
    </div>
</div>