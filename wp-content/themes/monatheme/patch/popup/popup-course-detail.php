        <div class="main-popup-header">
            <div class="logo">
                <?php echo get_custom_logo(); ?>
            </div>
            <p class="title"><?php the_title() ?></p>
        </div>
        <div class="main-popup-body">
            <div class="course-detail-intro">
                <div class="box-img">
                    <?php echo wp_get_attachment_image( get_field('image_intro_product'), '570x300') ?> 
                </div>
                <div class="box-content">
                    <div class="text-wrapper">
                        <?php the_content(  ) ?>    
                    </div>
                    <a href="#" data-id="<?php the_ID() ?>" class="main-btn w-auto mona-ajax-buy mona-btn-ajax">
                        <?php echo __('ENROL THIS COURSE','monamedia') ?>
                    </a>
                </div>
            </div>
            <div class="course-detail-info">
                <?php 
                $items = get_field('item_info_product_');
                if(is_array($items))
                {
                    foreach($items as $item)
                    {
                        ?>
                <div class="info-item">
                    <p class="info-title"><?php echo $item['title'] ?></p>
                    <div class="info-content">
                        <div class="text-wrapper">
                            <?php echo $item['content'] ?>
                        </div>
                    </div>
                </div>
                        <?php
                    }
                }
                ?>
                <div class="info-item">
                    <p class="info-title"><?php echo __("TIME AND VENUE",'monamedia') ?></p>
                    <div class="info-content">
                        <div class="schedule-wrapper">
                             <?php $items = get_field('time_and_venue');
                            if(is_array($items))
                            {
                                foreach($items as $item)
                                {
                                     ?>
                            <div class="schedule-item">
                                <label class="custom-radio">
                                    <?php $date_itm = $item['text_date']; ?>
                                    <?php  
                                    if($date_itm != '')
                                    {
                                        $array = explode(',',$date_itm);
                                        if(is_array($array) && count($array) > 0)
                                        { ?>
                                    <input type="radio" checked name="schedule" value="<?php echo $item['title'].' '.$date_itm ?>">
                                    <span class="checkmark"></span>
                                    <div class="schedule-content">
                                        <p class="schedule-title"><?php echo $item['title'] ?></p>
                                        <div class="schedule-date"> 
                                        <?php
                                            foreach($array as $itm)
                                            {
                                                echo '<span>'.$itm.'</span>';
                                            }
                                                    ?> 
                                        </div>
                                    </div>
                                    <?php
                                        }
                                    }
                                            ?> 
                                </label>
                            </div>         
                                     <?php
                                 }
                             }
                             ?>
                            
                        </div>
                    </div>
                </div>
                <?php global $product;  ?> 
                <div class="info-item">
                    <p class="info-title"><?php echo __('FEE','monamedia') ?></p>
                    <div class="info-content">
                        <div class="price-wrapper">
                            <p class="price"><?php echo $product->get_price_html(); ?></p>
                            <p class="per"><?php echo __('Per participant','monamedia')  ?></p>
                        </div>
                    </div>
                </div>
                 
            </div>
        </div>