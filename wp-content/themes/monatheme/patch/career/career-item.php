<div class="open-postion-item">
    <div class="path id">
        <p class="path-title"><?php echo __('Req ID','monamedia') ?>: <?php echo get_field('req_id_job') ?></p>
        <div class="path-content">
            <h3><a href="<?php the_permalink(  ) ?>"><?php the_title() ?></a></h3>
        </div>
    </div>
    <div class="path location">
        <p class="path-title"><?php echo __('Location','monamedia') ?></p>
        <div class="path-content"><?php echo get_field('location_job_single') ?></div>
    </div>
    <div class="path category">
        <p class="path-title"><?php echo __('Category','monamedia') ?></p>
        <div class="path-content"><?php echo get_field('category_job_single') ?></div>
    </div>
    <div class="path posted">
        <p class="path-title"><?php echo __('Posted','monamedia') ?></p>
        <div class="path-content"><?php the_time('M d, Y') ?></div>
    </div>
    <div class="path btn">
        <a href="<?php echo get_field('link_title_contact_us_job') ?>" class="main-btn"><?php echo __('CONTACT','monamedia') ?></a>
    </div>
</div>