            <div class="slide-item">        
                <div class="case-study-item">
                    <div class="case-study-img">
                        <a href="<?php the_permalink(  ) ?>" class="ratio-box">
                            <?php the_post_thumbnail( '470x313' ) ?>
                        </a>
                    </div>
                    
                    <div class="case-study-info">
                        <?php $vote = get_field('vote_case_single');
                        if($vote != '')
                        {
                            ?>
                        <div class="rating-stars">
                            <span class="empty-stars">
                                <i class="star fa fa-star"></i>
                                <i class="star fa fa-star"></i>
                                <i class="star fa fa-star"></i>
                                <i class="star fa fa-star"></i>
                                <i class="star fa fa-star"></i>
                            </span>
                            <span class="filled-stars" style="width: <?php echo ($vote * 2) ?>0%;">
                                <i class="star fa fa-star"></i>
                                <i class="star fa fa-star"></i>
                                <i class="star fa fa-star"></i>
                                <i class="star fa fa-star"></i>
                                <i class="star fa fa-star"></i>
                            </span>
                        </div>
                            <?php
                        } ?>
                        
                        <h4 class="case-study-title">
                            <a href="<?php the_permalink() ?>">
                                <strong><?php the_title() ?></strong><br>
                                <?php echo get_field('description_banner_case') ?>
                            </a>
                        </h4>
                        <ul class="case-study-nav">
                            <?php $fi = get_field('items_desc_case_single');
                            if(is_array($fi)) 
                                foreach($fi as $item)
                                    echo '<li> '.$item['text'].'</li>';
                            ?>
                            
                        </ul>
                        <a href="<?php the_permalink(  ) ?>" class="learn-more-btn"><?php echo __('LEARN MORE','monamedia') ?></a>
                    </div>
                </div>
            </div>