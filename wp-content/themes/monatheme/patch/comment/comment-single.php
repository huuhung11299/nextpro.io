                <div class="news-detail-comment">
                    <div class="main-title-box">
                        <h2 class="main-title"><?php echo __('Comment','monamedia') ?> <span>(<?php echo get_comments_number(  ) ?> <?php echo __('comments','monamedia') ?>)</span></h2>
                    </div>
                    <?php get_template_part( 'patch/comment/comment', 'form' );  ?> 

                    <?php get_template_part('patch/comment/comment', 'list') ?>
                </div>