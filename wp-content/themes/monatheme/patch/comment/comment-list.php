<div class="comment-wrapper">
 
    <?php 
    $args = array(
        'post_id' => get_the_ID(), 
    );
    $comment_page = get_comments( $args );  

    $array = array(
        'style' => 'ul',
        'short_ping' => true,
        'avatar_size' => 40,
        'callback' => 'mona_comment',
        'type' => 'all',
        'reply_text' => __('Reply', 'monamedia'),
        'page' => '',
        'per_page' => '',
        'reverse_top_level' => null,
        'reverse_children' => ''
    );
    wp_list_comments( $array ,$comment_page);


    function mona_comment($comment, $args, $depth){
        $comment_id = $comment->comment_ID; 
        $GLOBALS['comment'] = $comment;
        ?>
    <div class="comment-item" id="comment-<?php echo comment_ID(); ?>" <?php comment_class('cf'); ?>>
        <div class="comment-author">
            <div class="avatar"><img src="<?php echo get_site_url() ?>/template/images/user.svg" alt=""></div>
            <div class="info">
                <?php printf(__('<p class="name">%1$s</p>%2$s', 'monamedia'), get_comment_author_link(), edit_comment_link(__('(Edit)', 'monamedia'), '  ', '')) ?> 
              
                <p class="time"><?php comment_time(__('F jS, Y', 'monamedia')); ?></p>
            </div>
        </div>
        <div class="comment-content"><?php comment_text(); ?></div>
        <div class="comment-like-reply mona-like-ajax">
            <?php 
            $id_user = get_current_user_id(  );
            $check = get_comment_meta( get_comment_ID(), '_user_'.$id_user ,true ); ?>
            <a class="like like-comment <?php if($check == 'check') echo ' active '; ?> <?php if(!is_user_logged_in(  )) echo ' mona-action-login ' ?>"  data-id="<?php echo comment_ID(); ?>" href="javascript:;">Like <?php echo  get_comment_meta( get_comment_ID(), '_like_comment', true ); ?></a>
            <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?> 
        </div>
    </div>
    <!-- end item -->
        <?php
    }
     
    ?>
    
</div>
 