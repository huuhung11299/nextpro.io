<?php 
 $array = array(  
    // 'fields' => $fields,
    'class_submit' => 'main-btn',
    'logged_in_as' => '',
    'must_log_in' => '',
    'submit_button' => '<input type="submit" class="main-btn" value="SEND">',
    'title_reply_before' => '<h3 id="reply-title" class="comment-reply-title md-title">',
    'title_reply_after' => '</h3>',
    'title_reply' => __(''),
    'title_reply_to' => __('Leave a Reply to %s'),
    'comment_field' =>  ' 
    <input class="f-control" name="comment" placeholder="Your comment ...."></input>
'
);
?>

<div class="comment-form" id="comment-form">
    <?php if(is_user_logged_in(  )){
        comment_form($array);
    }else{ 
        echo '<a class="mona-action-login login-to-comment" href="#">'.__('LOGIN TO COMMENT','monamedia').'</a>';
    } ?> 
    
</div>