<div class="news-detail-share">
    <p class="title">SHARE</p>
    <ul class="nav">
        <li class="">
            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>&t=<?php the_title(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=500');
                    return false;" class="item facebook">
                <img src="<?php echo site_url(  ) ?>/template/images/share-fb.png" alt="">
            </a>
        </li>
         
        <li class="share-it">
            <a href="https://www.linkedin.com/cws/share?url=<?php echo urlencode(get_the_permalink()); ?>&title=<?php the_title(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=500');
                return false;" class="item linkedin">
                <img src="<?php echo site_url(  ) ?>/template/images/share-lk.png" alt="">
            </a>
        </li>

        <li class="share-it">
            <a href="http://www.twitter.com/share?url=<?php echo urlencode(get_the_permalink()); ?>" class="item twitter" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=500');
                    return false;">
                <img src="<?php echo site_url(  ) ?>/template/images/share-tw.png" alt=""> 
            </a>
        </li>
        

    </ul>
</div>
<!-- <div class="news-detail-share">
    <p class="title">SHARE</p>
    <ul class="nav">
        <li><a href="#"><img src="images/share-fb.png" alt=""></a></li>
        <li><a href="#"><img src="images/share-tw.png" alt=""></a></li>
        <li><a href="#"><img src="images/share-lk.png" alt=""></a></li>
        <li><a href="#"><img src="images/share-yt.png" alt=""></a></li>
    </ul>
</div> -->