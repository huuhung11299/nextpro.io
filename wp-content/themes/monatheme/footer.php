 
    <div class="footer">
        <div class="container">
            <div class="top-footer">
                <div class="columns">
                     <?php if(is_active_sidebar( 'footer_1' ))
                     {
                         dynamic_sidebar( 'footer_1' );
                     } ?>
                </div>
            </div>
            <div class="bottom-footer">
                <div class="columns">
                    <div class="column">
                        
                        <?php
                        $footer_logo = mona_get_option('mona_footer_logo');
                        if (isset($footer_logo) && $footer_logo != '') {
                            echo '<div class="ft-logo"><a href="' . get_home_url() . '"><img src="' . $footer_logo . '" alt=""></a></div>';
                        }
                        ?>
                        
                    </div>
                    <div class="column">
                        <div class="copyright"> <?php echo mona_get_option('mona_coppyright'); ?>    </div>
                    </div>
                    <div class="column">
                        <!-- <ul class="ft-menu">
                            <li><a href="#">Terms of Service</a></li>
                            <li><a href="#">Privacy</a></li>
                            <li><a href="#">We updated our Term of Service on April 24, 2019</a></li>
                        </ul> -->
                        <?php
                wp_nav_menu(array(
                    'container' => false,
                    'container_class' => 'nav-ul',
                    'menu_class' => 'ft-menu',
                    'theme_location' => 'footer-menu',
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'fallback_cb' => false,
                        // 'walker' => new Mona_Custom_Walker_Nav_Menu,
                ));
                ?>   
                        <?php get_template_part( 'patch/social','icon' ) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>    
        

    <span class="scroll-top"><i class="fa fa-angle-up"></i></span>
    <!-- <script src="js/jquery-1.12.4.min.js"></script> -->
    <script src="<?php echo site_url(); ?>/template/js/slick-1.8.1/slick.min.js"></script>
    <script src="<?php echo site_url(); ?>/template/js/Magnific-Popup-master/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo site_url(); ?>/template/js/lightGallery-master/dist/js/lightgallery-all.min.js"></script>
    <script src="<?php echo site_url(); ?>/template/js/WOW-master/wow.min.js"></script>
    <script src="<?php echo site_url(); ?>/template/js/main.js"></script>
    <?php wp_footer(); ?>
</body>
</html>