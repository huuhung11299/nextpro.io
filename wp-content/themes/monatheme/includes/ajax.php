<?php 

function mona_ajax_like_comment()
{
    $id_comment = @$_POST['data'];
    if($id_comment == '')
    {
        echo json_encode( array('status' => 'error' , 'mess' => 'khong co id' ) );
        wp_die();
    }
    
    $id_user = get_current_user_id(  );
    $check = get_comment_meta( $id_comment, '_user_'.$id_user, true );
    if($check == 'check')
    {
        echo json_encode( array('status' => 'error' , 'mess' => 'Like rồi' ) );
        wp_die();
    }
    else 
    {
        $like = get_comment_meta( $id_comment, '_like_comment', true );

        if($like == ''  )
        {
            $count = 1;
        } 
        else 
        {
            $count = (int) $like + 1;
        }
        $l = update_comment_meta( $id_comment, '_like_comment', (int) $count );
    
        if(!$l){
            echo json_encode( array('status' => 'error' , 'mess' => 'k update dc' ) );
            wp_die();
        }

        update_comment_meta( $id_comment, '_user_'.$id_user , 'check' );

        echo json_encode( array('status' => 'success' , 'mess' => $count ) );
        wp_die(); 
    }
    
} 
// add_action( 'wp_ajax_nopriv_mona_ajax_like_comment', 'mona_ajax_like_comment' );
add_action( 'wp_ajax_mona_ajax_like_comment',        'mona_ajax_like_comment' );

function mona_ajax_remove_like()
{
    $id_comment = @$_POST['data'];
    if($id_comment == '')
    {
        echo json_encode( array('status' => 'error' , 'mess' => 'khong co id' ) );
        wp_die();
    }
    $id_user = get_current_user_id(  );
    $check = get_comment_meta( $id_comment, '_user_'.$id_user, true );
    if($check == 'check')
    {
        update_comment_meta( $id_comment, '_user_'.$id_user , '');
        $like = get_comment_meta( $id_comment, '_like_comment', true );
        if($like == '' || $like == 0){
            echo json_encode( array('status' => 'error' , 'mess' => 'link == rong || link == 0' ) );
            wp_die();
        }else{ 
            $count = (int) $like - 1;
            $k = update_comment_meta( $id_comment, '_like_comment', $count  );
            if(!$k)
            {
                 
            }else{
                echo json_encode( array('status' => 'remove' , 'mess' => $count ) );
                wp_die();
            }
        }

    }
    else 
    {
        echo json_encode( array('status' => 'error' , 'mess' => 'chua like' ) );
        wp_die();
    }
}
// add_action( 'wp_ajax_nopriv_mona_ajax_remove_like', 'mona_ajax_remove_like' );
add_action( 'wp_ajax_mona_ajax_remove_like',        'mona_ajax_remove_like' );

function mona_load_course()
{
    $id_course  = @$_POST['data'];
    if($id_course == '') 
    {
        echo json_encode( array('status' => 'error' , 'mess' => 'k co id' ) );
        wp_die();
    }

    $array  = array( 
        'post_type' => 'product',
        'posts_per_page' => 1,
        'post__in' => array( $id_course ),
    );
    $query = new WP_Query($array);
    if($query->have_posts())
    {
        while($query->have_posts())
        {
            $query->the_post();
            get_template_part( 'patch/popup/popup', 'course-detail' );
        }wp_reset_query(  );
        wp_die();
    }else{ 
        echo json_encode( array('status' => 'error' , 'mess' => 'k co post' ) );
        wp_die();
    }

}
add_action( 'wp_ajax_nopriv_mona_load_course', 'mona_load_course' );
add_action( 'wp_ajax_mona_load_course',        'mona_load_course' );

function mona_buy_now()
{
    $id = @$_POST['data'];
    $time = @$_POST['time'];
    if($id == '' && $time  == '')
    {
        echo json_encode( array('status' => 'error' , 'mess' => 'k co id & time' ) );
        wp_die();
    }
    $cart_item_data  = array('time' => $time);
	wc()->cart->empty_cart();
    $cart = wc()->cart->add_to_cart($id , 1 , 0 , [],  $cart_item_data  );
    if($cart)
    {
        echo json_encode( array('status' => 'success' , 'mess' => wc_get_checkout_url() ) );
        wp_die();
    }
    echo json_encode( array('status' => 'error' , 'mess' => 'k add cart dc ' ) );
    wp_die( );
}
add_action( 'wp_ajax_nopriv_buy_now', 'mona_buy_now' );
add_action( 'wp_ajax_buy_now',        'mona_buy_now' );

function update_order_review() {
    $values = array();
    parse_str($_POST['data'], $values);
 
    $cart = $values['cart'];
    foreach ( $cart as $cart_key => $cart_value ){
        WC()->cart->set_quantity( $cart_key, $cart_value['qty'], false );
        WC()->cart->calculate_totals();
        woocommerce_cart_totals();
    }
    wp_die();
}
add_action( 'wp_ajax_nopriv_update_order_review', 'update_order_review' );
add_action( 'wp_ajax_update_order_review',        'update_order_review' );