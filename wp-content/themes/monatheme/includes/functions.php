<?php
function mona_page_navi($wp_query='') {
	if($wp_query==''){
	global $wp_query;	
	}
    
    $bignum = 999999999;
    if ($wp_query->max_num_pages <= 1)
        return;
    echo '<nav class="pagination">';
    echo paginate_links(array(
        'base' => str_replace($bignum, '%#%', esc_url(get_pagenum_link($bignum))),
        'format' => '',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'prev_text' => '&larr;',
        'next_text' => '&rarr;',
        'type' => 'list',
        'end_size' => 3,
        'mid_size' => 3
    ));
    echo '</nav>';
}
function mona_page_navi_url($wp_query = '', $url = '') {
    if ($wp_query == '') {
        global $wp_query;
    }
    $bignum = 999999999;
    if ($url == '') {
        $url = str_replace($bignum, '%#%', esc_url(get_pagenum_link($bignum)));
    }

    if ($wp_query->max_num_pages <= 1)
        return;
    echo '<nav class="pagination">';
    echo paginate_links(array(
        'base' => $url,
        'format' => '',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'prev_text' => '&larr;',
        'next_text' => '&rarr;',
        'type' => 'list',
        'end_size' => 3,
        'mid_size' => 3
    ));
    echo '</nav>';
}
function mona_get_percent($id_product){ 
    $product = wc_get_product( $id_product );
    if(!$product->is_on_sale()){
        return;
    }
    $regular = $product->get_regular_price();
    $sale = $product->get_sale_price();
    $percent =  100 - (($sale / $regular) * 100 );
    return round($percent, 1);
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_first_name']);
    $fields['billing']['billing_last_name']['label'] = 'Full name';
    //unset($fields['billing']['billing_last_name']);
    // unset($fields['billing']['billing_company']);
    // unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    // unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
    // unset($fields['billing']['billing_country']);
    // unset($fields['billing']['billing_state']);
    // unset($fields['billing']['billing_phone']);
    // unset($fields['order']['order_comments']);
    // unset($fields['billing']['billing_email']);
    // unset($fields['account']['account_username']);
    // unset($fields['account']['account_password']);
    // unset($fields['account']['account_password-2']); 
$fields['billing']['custom_field_gender']=  array(

            'type' => 'radio',
            'label_class' => array('f-group-title'),
            'class' => array(

            'my-field-class form-row-wide f-group-wrapper'

        ) ,

        'label' => __('Gender', 'monamedia') ,
        'default'=> 'Male',
        'required'=> true,

        'options'     => array(
            'Male' => __('Male', 'monamedia'),
            'Female' => __('Female', 'monamedia')
        ),

    );
	$fields['billing']['custom_field_registration_mode']=  array(

            'type' => 'radio',
            'label_class' => array('f-group-title'),
            'class' => array(

            'my-field-class form-row-wide f-group-wrapper'

        ) ,

        'default'=> 'personal',
        'required'=> true,

        'label' => __('Registration Mode', 'monamedia') ,

        'options'     => array(
            'personal' => __('Personal', 'monamedia'),
            'corporate' => __('Corporate', 'monamedia')
        ),

    );
	$fields['billing']['custom_field_position']=  array(

            'type' => 'text',
 'label_class' => array('f-group-title'),
            'class' => array(

            'my-field-class form-row-wide f-group-wrapper'

        ) ,

        'required'=> true,

        'label' => __('Your Position', 'monamedia') ,
 
        'placeholder' => __('Your Position', 'monamedia') ,
    );
	$fields['billing']['custom_field_company_tax_code']=  array(

            'type' => 'text',
            'label_class' => array('f-group-title'),

            'class' => array(

            'my-field-class form-row-wide f-group-wrapper'

        ) ,

        'required'=> true,

        'label' => __('Your Company Tax Code', 'monamedia') ,
                
        'placeholder' => __('Your Company Tax Code', 'monamedia') ,
    ) ;
	$priority=0;
    return $fields;
}


function mona_enqueue_comments_reply() {
    if (get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('comment_form_before', 'mona_enqueue_comments_reply');

function hide_admin_bar($show) {
    if (current_user_can('administrator')) {
        return $show;
    }
    return false;
}

add_filter('show_admin_bar', 'hide_admin_bar');

function mona_remove_admin_page(){
    if(wp_doing_ajax()){
        return;
    }
    if (is_user_logged_in() && !current_user_can('administrator')) {
        wp_redirect(get_home_url());
    } 
}
add_action('admin_init', 'mona_remove_admin_page');

// add_filter( 'woocommerce_add_cart_item_data', 'woo_custom_add_to_cart' );

function woo_custom_add_to_cart( $cart_item_data ) {

    global $woocommerce;
    $woocommerce->cart->empty_cart();

    // Do nothing with the data and return
    return $cart_item_data;
}
 


add_action( 'woocommerce_checkout_update_order_meta', 'mona_checkout_field_update_order_meta' );

function mona_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['custom_field_gender'] ) ) {
        update_post_meta( $order_id, '_custom_field_gender', sanitize_text_field( $_POST['custom_field_gender'] ) );
    }
    if ( ! empty( $_POST['custom_field_registration_mode'] ) ) {
        update_post_meta( $order_id, '_custom_field_registration_mode', sanitize_text_field( $_POST['custom_field_registration_mode'] ) );
    }
    if ( ! empty( $_POST['custom_field_position'] ) ) {
        update_post_meta( $order_id, '_custom_field_position', sanitize_text_field( $_POST['custom_field_position'] ) );
    }
    if ( ! empty( $_POST['custom_field_company_tax_code'] ) ) {
        update_post_meta( $order_id, '_custom_field_company_tax_code', sanitize_text_field( $_POST['custom_field_company_tax_code'] ) );
    }
}

function mona_field_function_name($order){
    $id = ($order->get_data());
    $id = $id['id'];
    echo '<p><strong>' . __('Fender','monamedia') . ': </strong>' . get_post_meta($id, '_custom_field_gender', true) . '</p>';
    echo '<p><strong>' . __('Registration Mode','monamedia') . ': </strong> ' . get_post_meta($id, '_custom_field_registration_mode', true) . '</p>';
    echo '<p><strong>' . __('Position','monamedia') . ': </strong>' . get_post_meta($id, '_custom_field_position', true) . '</p>';
    echo '<p><strong>' . __('Company Tax Code','monamedia') . ': </strong>' . get_post_meta($id, '_custom_field_company_tax_code', true) . '</p>';
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'mona_field_function_name', 10, 1 );

function mona_display_engraving_text_cart( $item_data, $cart_item ) {
	if ( empty( $cart_item['time'] ) ) {
		return $item_data;
	}

	$item_data[] = array(
		'key'     => __( 'Date', 'monamedia' ),
		'value'   => wc_clean( $cart_item['time'] ),
		'display' => '',
	);

	return $item_data;
}

add_filter( 'woocommerce_get_item_data', 'mona_display_engraving_text_cart', 10, 2 );

add_action( 'woocommerce_checkout_create_order_line_item', 'save_cart_item_custom_meta_as_order_item_meta', 10, 4 );
function save_cart_item_custom_meta_as_order_item_meta( $item, $cart_item_key, $values, $order ) {
    $meta_key = 'TIME';
    if ( isset($values['time'])  ) {

        $item->update_meta_data( $meta_key, $values['time'] );

    }
}