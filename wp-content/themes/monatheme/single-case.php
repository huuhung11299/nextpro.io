<?php
get_header();
while (have_posts()):
    the_post();
    ?>

    <main class="main">
        <div class="case-study-banner-section sec" style="background-image: url(<?php echo site_url(  ) ?>/template/images/case-study-banner.jpg);">
            <div class="container">
                <h1 class="case-study-banner-title"><?php echo __("Case Study", 'monamedia') ?></h1>
                <div class="case-study-banner-desc">
                   <?php echo get_field('description_banner_case') ?>
                </div>
                <a href="<?php echo get_field('link_btn_banner_case') ?>" class="main-btn"><?php echo get_field('title_button_banner_case') ?></a>
            </div>
        </div>
        <div class="like-section sec">
            <div class="container">
                <p class="like-section-title"><?php echo get_field('title_bar_case') ?></p>
                <ul class="like-section-nav">
                    <?php 
                    $social = get_field('socical_bar_case');
                    if(is_array($social)) 
                        foreach($social as $item)
                            echo '<li><a href="'.$item['link'].'">'.$item['title'].'</a></li>';
                    ?>
                    
                </ul>
            </div>
        </div>
        <?php $slider = get_field('image_content_case_single');
        if(is_array($slider) && count($slider) > 0)
        { ?>
        
        
        <div class="about-direct-section sec">
            <div class="about-direct-slider-nav main-slider main-slider-stretch arrow-inside">
            <?php
             
            foreach($slider as $items)
            {
                ?>
                <div class="slide-item mona-btn-show-img" data-img="<?php echo wp_get_attachment_image_url($items['image'],'full')  ?>" >
                    <div class="about-direct-link">
                        <p class="title"><?php echo $items['title'] ?></p>
                        <div class="desc"><?php echo $items['description'] ?></div>
                        <div class="sub-desc">
                            <ul class="sub-desc-nav">
                                <?php 
                                $fi = $items['items_text'];
                                if(is_array($fi))
                                    foreach($fi as $item)
                                        echo '<li>'.$item['text'].'</li>';
                                ?>  
                            </ul>
                            <a href="#" data-img="<?php echo wp_get_attachment_image_url($items['image'],'full')  ?>" 
                            class="main-btn mona-btn-show-img"><?php echo __("LEARN MORE",'monamedia') ?></a>
                        </div>
                    </div>
                </div>       
                <?php 
            }   
                    ?> 
                
            </div>
            <div class="about-direct-slider main-slider main-slider-stretch arrow-inside">
            <?php
                foreach($slider as $items)
                {
                    ?>
                <div class="slide-item">
                    <div class="about-direct-banner" 
                    style="background-image: url(<?php echo wp_get_attachment_image_url( $items['background'],'1920x575' ) ?>);">
                        <div class="container">
                            <h2 class="about-direct-banner-title"><?php echo $items['title'] ?></h2>
                            <ul class="about-direct-banner-nav">
                            <?php 
                                $fi =  $items['items_text'];
                                if(is_array($fi))
                                    foreach($fi as $item)
                                        echo '<li>'.$item['text'].'</li>';
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>    
                    <?php

                }    ?>
                
            </div>
        </div>    
            <?php
        }
        ?>
        <div class="case-study-schedule-section sec">
            <div class="container">
                <div class="full-img show-image"> 
                     
                </div>
            </div>
        </div>
        <div class="case-study-schedule-section">
            <div class="container mona-content">
               
                <?php the_content(  ) ?>
             
            </div>
        </div>
    </main>
    
    <?php
endwhile;
get_footer();
?>
<script>
jQuery(document).ready(function($){
    $(".mona-btn-show-img").on('click',function(e){
        e.preventDefault();
        let data = $(this).attr('data-img'),
        html = '<img src="'+data+'" alt="">';
        $('.show-image').html(html); 
    })
});
</script>

