<?php
if(get_current_user_id(  ) != 1 ){
    define('ACF_LITE', true);
}
define('MONA_BLOG', 302);
define('MONA_CAREER', 353);

require_once( get_template_directory() . '/core/class/core.class.php' );
require_once( get_template_directory() . '/core/class/Mona_walker.php' );
require_once( get_template_directory() . '/core/class/hook.class.php' );
require_once( get_template_directory() . '/core/customizer.php' );
require_once( get_template_directory() . '/includes/functions.php' );
require_once( get_template_directory() . '/includes/ajax.php' );

// image size register
function mona_image_size() {
    //  add_image_size('slider-full', 1900, 790, true);
    add_image_size('1920x590'   , 1920  , 590   , true);
    add_image_size('1920x575'   , 1920  , 575   , true);
    add_image_size('1920x400'   , 1920  , 400   , true);
    add_image_size('1360x1360'  , 1360  , 1360  , true);
    add_image_size('1170x430'   , 1170  , 430   , true);
    add_image_size('680x536'    , 680   , 536   , true);
    add_image_size('570x300'    , 570   , 300   , true);
    add_image_size('470x313'    , 470   , 313   , true); 
    add_image_size('470x270'    , 470   , 270   , true); 
    add_image_size('370x230'    , 370   , 230   , true);
    add_image_size('370x240'    , 370   , 240   , true);
    add_image_size('100x100'    , 100   , 100   , true);
}
add_action('after_setup_theme', 'mona_image_size');

function mona_register_menu() {
    register_nav_menus(
        [
            'primary-menu' => __('Theme Main Menu', 'monamedia'),
            'footer-menu' => __('Theme Footer Menu', 'monamedia'),
            'top-menu' => __('Theme Top Menu', 'monamedia'),
        ]
    );
}

add_action('after_setup_theme', 'mona_register_menu');

function mona_register_sidebars() {
    // register_sidebar(array(
    //     'id' => 'sidebar_page',
    //     'name' => __('sidebar page Term & Conditions', 'mona_media'),
    //     'description' => __('The first (primary) sidebar.', 'mona_media'),
    //     'before_widget' => '<div id="sidebar" class="sidebar">',
    //     'after_widget' => '</div>',
    //     'before_title' => '',
    //     'after_title' => '',
    // ));
    register_sidebar(array(
        'id' => 'sidebar_page_faqs',
        'name' => __('sidebar page FAQs', 'mona_media'),
        'description' => __('The first (primary) sidebar.', 'mona_media'),
        'before_widget' => '<div id="sidebar" class="sidebar">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => '',
    ));
    register_sidebar(array(
        'id' => 'footer_1',
        'name' => __('Footer 1', 'mona_media'),
        'description' => __('The first footer.', 'mona_media'),
        'before_widget' => '<div class="column">',
        'after_widget' => '</div>',
        'before_title' => '<p class="ft-title">',
        'after_title' => '</p>',
    ));
    
    // register widget item
     // require_once(get_template_directory() . '/widget/mona-cart.php');
  //  register_widget('mona_cart');
}

add_action('widgets_init', 'mona_register_sidebars');

function mona_style() {
    wp_enqueue_style('mona-custom', get_template_directory_uri() . '/css/mona-custom.css');

    wp_enqueue_script('comment-reply', get_template_directory_uri() . '/js/libs.js'); 

    wp_enqueue_script('mona-swelt', get_template_directory_uri() . '/js/sweetalert.min.js', array(), false, true);
    wp_enqueue_script('mona-countup', get_template_directory_uri() . '/js/countUp/jquery.min.js', array(), false, true);
    wp_enqueue_script('mona-countup-min', get_template_directory_uri() . '/js/countUp/waypoints.min.js', array(), false, true);

    wp_enqueue_script('mona-front', get_template_directory_uri() . '/js/front.js', array(), false, true);
    wp_localize_script('mona-front', 'mona_ajax_url', array('ajaxURL' => admin_url('admin-ajax.php'), 'siteURL' => get_site_url()));
}

add_action('wp_enqueue_scripts', 'mona_style');

function add_menu_parent_class($items) {
    $parents = array();
    foreach ($items as $item) {
        //Check if the item is a parent item
        if ($item->menu_item_parent && $item->menu_item_parent > 0) {
            $parents[] = $item->menu_item_parent;
        }
    }

    foreach ($items as $item) {
        if (in_array($item->ID, $parents)) {
            //Add "menu-parent-item" class to parents
            $item->classes[] = 'dropdown';
        }
    }

    return $items;
}

add_filter('wp_nav_menu_objects', 'add_menu_parent_class');

function mona_add_custom_post() {
    $args = array(
        'labels' => array(
            'name' => 'Job',
            'singular_name' => 'Job',
            'add_new' => __('Add Job', 'monamedia'),
            'add_new_item' => __('New Job', 'monamedia'),
            'edit_item' => __('Edit Job', 'monamedia'),
            'new_item' => __('New Job', 'monamedia'),
            'view_item' => __('View Job', 'monamedia'),
            'view_items' => __('View Job', 'monamedia'),
        ),
        'description' => 'Add Job',
        'supports' => array(
            'title',
            'editor',
            'author',
            'thumbnail',
            'comments',
            'revisions',
            'custom-fields'
        ),
        'taxonomies' => array(),
        'hierarchical' => false,
		'show_in_rest' => true,
        'public' => true,
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'job',
            'with_front' => true
        ),
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-groups',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post'
    );
    register_post_type('job', $args); 

    $args = array(
        'labels' => array(
            'name' => 'Case',
            'singular_name' => 'Case',
            'add_new' => __('Add Case', 'monamedia'),
            'add_new_item' => __('New Case', 'monamedia'),
            'edit_item' => __('Edit Case', 'monamedia'),
            'new_item' => __('New Case', 'monamedia'),
            'view_item' => __('View Case', 'monamedia'),
            'view_items' => __('View Case', 'monamedia'),
        ),
        'description' => 'Add Case',
        'supports' => array(
            'title',
            'editor',
            'author',
            'thumbnail',
            'comments',
            'revisions',
            'custom-fields'
        ),
        'taxonomies' => array(),
        'hierarchical' => false,
		'show_in_rest' => true,
        'public' => true,
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'case',
            'with_front' => true
        ),
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-list-view',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post'
    );
    register_post_type('case', $args);

    //Course
    $args = array(
        'labels' => array(
            'name' => 'Course',
            'singular_name' => 'Course',
            'add_new' => __('Add Course', 'monamedia'),
            'add_new_item' => __('New Course', 'monamedia'),
            'edit_item' => __('Edit Course', 'monamedia'),
            'new_item' => __('New Course', 'monamedia'),
            'view_item' => __('View Course', 'monamedia'),
            'view_items' => __('View Course', 'monamedia'),
        ),
        'description' => 'Add Course',
        'supports' => array(
            'title',
            'editor',
            'author',
            'thumbnail',
            'comments',
            'revisions',
            'custom-fields'
        ),
        'taxonomies' => array(),
        'hierarchical' => false,
		'show_in_rest' => true,
        'public' => true,
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'course',
            'with_front' => true
        ),
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-admin-page',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post'
    );
    // register_post_type('course', $args); 

    $tax_args = array(
        'labels' => array(
            'name' => __('Jobs', 'monamedia'),
            'singular_name' => __('Jobs', 'monamedia'),
            'search_items' => __('Search Jobs', 'monamedia'),
            'all_items' => __('All Jobs', 'monamedia'),
            'parent_item' => __('Parent Jobs', 'monamedia'),
            'parent_item_colon' => __('Parent Jobs', 'monamedia'),
            'edit_item' => __('Edit Jobs', 'monamedia'),
            'add_new' => __('Add Jobs', 'monamedia'),
            'update_item' => __('Update Jobs', 'monamedia'),
            'add_new_item' => __('Add New Jobs', 'monamedia'),
            'new_item_name' => __('New Jobs Name', 'monamedia'),
            'menu_name' => __('Jobs', 'monamedia'),
        ),
        'hierarchical' => true,
        'has_archive' => true,
        'public' => true,
        'rewrite' => array(
            'slug' => 'jobs',
            'with_front' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'publish_posts',
            'edit_terms' => 'publish_posts',
            'delete_terms' => 'publish_posts',
            'assign_terms' => 'publish_posts',
        ),
    );
    // register_taxonomy('jobs', 'job', $tax_args);

    flush_rewrite_rules();
}

add_action('init', 'mona_add_custom_post');

function mona_overwrite_billing_in_theme($fields) {
  //  var_dump($fields['order']['order_comments']);
   $fields['order']['order_comments']['label']='Messages';
   $fields['order']['order_comments']['class'][]='f-group-wrapper';
   $fields['order']['order_comments']['label_class'][]='f-group-title';
        $priority = 1;
        $fields['billing']['billing_company']['required']=true;
        foreach ($fields['billing'] as $k=>$item){
            $fields['billing'][$k]['class'][]='f-group-wrapper';
            
            $fields['billing'][$k]['label_class'][]='f-group-title';
        }
        return $fields;
    }
    add_filter('woocommerce_checkout_fields','mona_overwrite_billing_in_theme', 9999);
    
    function mona_order_btn_filter($text){
        return 'PAY NOW';
    }
    
    add_filter( 'woocommerce_order_button_text',  'mona_order_btn_filter' ); 