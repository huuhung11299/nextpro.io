<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @author : monamedia
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <title><?php wp_title('|', true, 'right'); ?></title>
        <!-- Meta
                ================================================== -->
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, width=device-width">
        <?php wp_site_icon(); ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
        <link rel="stylesheet" href="<?php echo get_site_url() ?>/template/css/style.css"/>
        <link rel="stylesheet" href="<?php echo get_site_url() ?>/template/css/responsive.css" media="all"/>
        <?php wp_head(); ?>
        </head>
<?php
    if(wp_is_mobile()){
        $body = 'mobile-detect';
    }else{
       $body = 'desktop-detect'; 
    }
?>
<body <?php body_class($body); ?>> 
    <header class="header">
        <div class="container">
            <div class="hamburger-btn hd-cate-btn">
                <div class="bar"></div>
            </div>
            <div class="hd-logo">
                <?php echo get_custom_logo(); ?>
            </div>
            <nav class="hd-cate">
                <!-- <ul class="hd-cate-nav">
                    <li><a href="#">MARKET RESEARCH</a></li>
                    <li><a href="#">RFX</a></li>
                <li><a href="#">E-AUCTION</a></li> -->
            </ul>
            <?php
                wp_nav_menu(array(
                    'container' => false,
                    'container_class' => 'nav-ul',
                    'menu_class' => 'hd-cate-nav',
                    'theme_location' => 'top-menu',
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'fallback_cb' => false,
                        // 'walker' => new Mona_Custom_Walker_Nav_Menu,
                ));
                ?>   
                <div class="hd-cate-overlay"></div>
            </nav>
            <div class="hd-right">
                <div class="hd-right-item hide-mobile">
                    <a href="<?php echo home_url(  ) ?>"><img src="<?php echo get_site_url() ?>/template/images/home.svg" alt=""></a>
                </div>
                <div class="hd-right-item">
                    <ul class="hd-right-nav">
                        <li class="">
                            <?php $phone = mona_get_option('mona_header_phone'); ?>
                            <a href="tel:<?php echo $phone ?>"><img src="<?php echo get_site_url() ?>/template/images/phone.svg" alt=""><?php echo $phone ?></a>
                        </li>
                        <li>
                            <div class="language-picker">
                                <span class="current"><img src="<?php echo get_site_url() ?>/template/images/earth.svg" alt="">EN</span>
                                <ul class="sub-menu">
                                    <li><a href="#"><img src="<?php echo get_site_url() ?>/template/images/earth.svg" alt="">EN</a></li>
                                    <li><a href="#"><img src="<?php echo get_site_url() ?>/template/images/earth.svg" alt="">EN</a></li>
                                </ul>
                            </div>
                        </li>
                        <?php if(is_user_logged_in(  )){
                              $user_data  = (wp_get_current_user()->data); 
                            ?>
                        <li class="info-user">
                            <div class="language-picker">
                                <span class="current"><img src="<?php echo get_site_url() ?>/template/images/user.svg" alt=""> <p class="hide-mobile"><?php echo $user_data->display_name ?></p></span>
                                <ul class="sub-menu">
                                    <li> <a href="javascript:;"><?php echo $user_data->user_email ?></a></li>
                                    <li><a href="#" class="mona-logout-action"> Logout </a></li>
                                </ul>
                            </div>
                        </li>      
                            <?php
                        }else{
                            ?>
                        <li><a class="mona-action-login" href="#"><img src="<?php echo get_site_url() ?>/template/images/user.svg" alt=""><?php echo __('SIGN IN','monamedia') ?></a></li>      
                            <?php
                        } ?>
                    </ul>
                </div>
                <div class="hd-right-item">
                    <div class="hamburger-btn main-menu-btn">
                        <div class="bar"></div>
                    </div>
                    <nav class="main-menu"> 
                        <?php
                        wp_nav_menu(array(
                            'container' => false,
                            'container_class' => 'nav-ul',
                            'menu_class' => 'main-menu-nav',
                            'theme_location' => 'primary-menu',
                            'before' => '',
                            'after' => '',
                            'link_before' => '',
                            'link_after' => '',
                            'fallback_cb' => false,
                                'walker' => new Mona_Custom_Walker_Nav_Menu,
                        ));
                        ?>   
                        <div class="main-menu-overlay"></div>
                    </nav>
                </div>
            </div>
        </div>
    </header>