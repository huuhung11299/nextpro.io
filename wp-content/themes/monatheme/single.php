<?php
get_header();
while (have_posts()):
    the_post();
    ?>
    <main class="main">
        <div class="banner-section sec" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field('banner_background_page') ,  '1920x590'); ?>);">
            <div class="container">
                <div class="banner-title"></div> <!-- không xoá -->
                <div class="banner-bottom">
                    <p class="banner-title"><?php the_title() ?> </p>
                    <p class="banner-news-author"><?php the_time( 'M d, Y' ) ?>  By <a href="<?php echo get_author_posts_url(get_the_author_meta('ID'));; ?>"><strong><?php echo get_the_author_meta( 'nickname' ) ?></strong></a></p>
                </div>
            </div>
        </div>
        <div class="news-detail-section sec">
            <div class="container">
                <div class="news-detail-content">
                    <div class="intro"><?php the_excerpt(  ) ?></div>
                    <div class="full-img">
                        <?php echo wp_get_attachment_image( get_field('image_single_post') , '1170x430' ) ?>
                    </div>
                    <div class="mona-content-wrapper">
                        <div class="columns">
                            <div class="column left">
                                <div class="mona-content">
                                     <?php the_content( ) ?>
                                </div>
                            </div>
                            <div class="column right">
                                <div class="mona-content-quote"> <?php echo get_field('content_quote_single_post') ?></div>
                            </div>
                        </div>
                    </div>
                    <?php get_template_part( 'patch/social', 'share' ) ?>
                </div>
                <?php get_template_part( 'patch/comment/comment', 'single' ) ?>
            </div>
        </div>
    </main>
    <?php
endwhile;
get_footer();
?>