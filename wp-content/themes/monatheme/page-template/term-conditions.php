<?php
/**
 * Template name: Term Conditions 
 * @author : Hy Hý
 */
//  W-4.html
get_header();
while (have_posts()):
    the_post();
    ?>
     
 
    <main class="main">
        <div class="banner-section sec" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field('banner_background_page') ,  '1920x590'); ?>);">
            <div class="container">
                <h1 class="banner-title mt-auto"><?php the_title() ?></h1>
            </div>
        </div>
        <?php 
        $current = 'current';
        $slider = get_field('slider_items_terms_page');
        if(is_array($slider))
        { 
                ?>
        <div class="tab-wrapper">
            <div class="term-direct-slider-nav main-slider main-slider-stretch arrow-inside">
                <?php 

                foreach($slider as $key =>  $item)
                {
                    ?>
                <div class="slide-item tab-link <?php echo  $current ?>" data-tab="#main-section-<?php echo $key ?>">
                    <div class="about-direct-link">
                        <p class="title"><?php echo $item['title'] ?></p>
                    </div>
                </div>    
                    <?php
                    $current = '';
                } ?>
                
            </div>
            <div class="main-section-wrap">
                <?php foreach($slider as $key =>  $item)
                {
                    ?>
                <div id="main-section-<?php echo $key ?>" class="main-section sec">
                    <div class="container">
                        <div class="tab-wrapper">
                            <div class="columns">
                                <?php 
                                $tab =  $item['box_content'];
                                if(is_array($tab))
                                {
                                    ?>
                                <div class="column main-section-left">
                                    <div class="sidebar">
                                        <ul class="sidebar-nav">
                                            <?php 
                                            $current = 'current';
                                            foreach($tab as $ky => $itm)
                                            {
                                               echo '<li class="tab-link '.$current.'" data-tab="#faqs-'.$ky.'"><a href="#">'.$itm['title'].'</a></li>';
                                               $current = '';
                                            } ?> 
                                        </ul>
                                    </div>
                                </div>
                                <div class="column main-section-right">
                                    <?php 
                                    foreach($tab as $ky => $itm)
                                    { //faqs-container
                                    ?>
                                    <div class="" id="faqs-<?php echo $ky ?>">
                                         
                                        <div class="faqs-wrapper">
                                            <?php $active = 'active';
                                            $content  = $itm['content_field'];
                                            if(is_array($content))
                                            {
                                                foreach($content as $k => $text)
                                                {
                                                    ?> 
                                            <div class="faqs-item <?php echo $active ?>">
                                                <h3 class="faqs-title"><?php echo $text['title'] ?></h3>
                                                <div class="faqs-content">
                                                    <?php echo $text['content']; ?>
                                                </div>
                                            </div>       
                                                    <?php $active = '';
                                                }
                                            }
                                            ?> 
                                        </div>
                                    </div> 
                                    <?php 
                                    } ?>
                                </div>
                                    <?php
                                }
                                ?>
                               
                            </div>
                        </div>
                    </div>
                </div>    
                    <?php
                } ?> 
            </div>
        </div>        
                <?php 
        }
        ?>
        
    </main>
   
    <?php
endwhile;
get_footer();
?>