<?php
/**
 * Template name: FAQs Page
 * @author : Hy Hý
 */
// W-8.html
get_header();
while (have_posts()):
    the_post();
    ?>
    <main class="main">
        <div class="banner-section sec" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field('banner_background_page') ,  '1920x590'); ?>);">
            <div class="container">
                <h1 class="banner-title mt-auto"><?php the_title() ?></h1>
            </div>
        </div>
        <div id="main-section" class="main-section sec">
            <div class="container">
                <div class="tab-wrapper">
                    <div class="columns">
                        <?php 
                        $faqs = get_field('items_faqs_page_2');
                        if(is_array($faqs))
                        {
                            ?>
                        <div class="column main-section-left">
                            <div id="sidebar" class="sidebar">
                                <ul class="sidebar-nav">
                                    <?php $current = 'current'; 
                                    foreach($faqs as $key=> $item) 
                                        echo '<li class="tab-link '.$current.' ?>" data-tab="#faqs-'.$key.'"><a href="#">'.$item['title'].'</a></li>';
                                    ?>   
                                </ul>
                            </div>
                        </div>
                        <div class="column main-section-right">
                            <?php
                            foreach($faqs as $key=> $item)
                            {
                                ?>
                            <div id="faqs-<?php echo $key ?>" class="faqs-wrapper">
                                <?php $content = $item['content'];
                                if(is_array($content))
                                {
                                    $active = 'active';
                                    foreach($content as $itm)
                                    {
                                        ?>
                                <div class="faqs-item <?php echo $active ?>">
                                    <h3 class="faqs-title"><?php echo $itm['title'] ?></h3>
                                    <div class="faqs-content">
                                        <?php echo $itm['content']; ?>
                                    </div>
                                </div>        
                                        <?php
                                        $active = '';
                                    }
                                } 
                                ?> 
                            </div>    
                                <?php
                            }   ?> 
                        </div>    
                            <?php
                        }
                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php
endwhile;
get_footer();
?>