<?php
/**
 * Template name: Procurement Services
 * @author : Hy Hý
 */
// W-1.2.html , W-1.1.html , W-1.4.html , W-1.3.html => done 
get_header();
while (have_posts()):
    the_post();
    ?>
    <main class="main">
        <div class="w-banner-section sec">
            <?php $slider = get_field('items_slider_managed');
            if(is_array($slider)){
                ?>
            <div class="w-banner-slider main-slider main-slider-stretch">
                <?php foreach($slider as $item){
                    ?>
                 <div class="slide-item">
                    <div class="w-banner-item" style="background-image: url(<?php echo wp_get_attachment_image_url( $item['background'] , '1920x575' ) ?>);">
                        <div class="container">
                            <p class="w-banner-title wow fadeInDown"><?php echo str_replace('|','<br>',$item['title']) ?></p>
                        </div>
                    </div>
                </div>    
                    <?php
                } ?> 
                 
            </div>
            <div class="w-banner-slider-nav main-slider main-slider-stretch wow fadeInUp">
                <?php foreach($slider as $item){
                    ?>
                <div class="slide-item">
                    <div class="title"><?php echo $item['sub_title'] ?></div>
                </div>    
                    <?php
                } ?>
                
            </div>    
                <?php
            }  ?>
            
        </div>

        <?php get_template_part( 'patch/section/section','service' ) ?>

        <?php get_template_part( 'patch/section/section', 'why' ) ?>

        <section class="solutions-section sec ">
            <div class="container">
                <div class="solutions-content fadeInLeft wow">
                    <div class="main-title-box ">
                        <h2 class="main-title"><?php echo get_field('title_solutions') ?></h2>
                    </div>
                    <div class="text-wrapper"><strong><?php echo get_field('content_solutions') ?></strong></div>
                    <div class="plan-wrapper">
                        <div class="columns">
                            <?php 
                            $column = get_field('items_solutions');
                            if( is_array($column) )
                            {
                                foreach($column as $item)
                                {
                                    ?>
                            <div class="column">
                                <div class="text-wrapper">
                                    <strong><?php echo $item['title'] ?>:</strong> <?php echo $item['content'] ?>
                                </div>
                            </div>        
                                    <?php
                                }
                            }
                            ?> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="level-section sec wow fadeInRight">
            <div class="container">
                <div class="main-title-box">
                    <h2 class="main-title"><?php echo get_field('title_bar') ?></h2>
                </div>
                <div class="text-wrapper"><?php echo get_field('content_bar') ?></div>
            </div>
        </section>
        
        <?php get_template_part( 'patch/section/section', 'case-study' ) ?>

        <section class="figures-section sec" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field('background_figures') , 'full' ) ?>);">
            <div class="container">
                <div class="main-title-box wow fadeInLeft">
                    <h2 class="main-title-2"><?php echo get_field('title_figures'); ?></h2>
                </div>
                <div class="figures-wrapper ">
                    <div class="columns">
                        <?php 
                        $column = get_field('items_figures');
                        if(is_array($column))
                        {
                            foreach($column as $k => $item) 
                            {
                                ?>
                        <div class="column wow fadeInRight" data-wow-delay="0.<?php echo $k * 2 ?>s">
                            <div class="figures-item">
                                <p class="number">+<span class="countUp-number"><?php echo $item['number'] ?></span> <?php echo $item['text'] ?></p>
                                <p class="desc"><?php echo $item['description'] ?></p>
                            </div>
                        </div>        
                                <?php
                            }
                        }
                        ?>
                        
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
endwhile;
get_footer();
?>