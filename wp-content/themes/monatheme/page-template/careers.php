<?php
/**
 * Template name: Careers Page
 * @author : Hy Hý
 */
// W-5.html
get_header();
while (have_posts()):
    the_post();
    ?>
    <main class="main">
        <div class="banner-section sec" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field('image_banner_careers') ,'1920x590') ?>);">
            <div class="container">
                <h1 class="banner-title mt-auto"><?php the_title() ?></h1>
            </div>
        </div>
        <section class="open-position-section sec">
            <div class="container">
                <div class="main-title-box">
                    <h2 class="main-title-3"><?php echo get_field('subtitle_page_careers') ?></h2>
                    <div class="sub-title"> 
                        <?php the_content(  ) ?>
                    </div>
                </div>
                <div class="open-position-search-form">
                    <form action="" id="form-search-job" method="GET">
                        <input type="text" name="key" value="<?php echo @$_GET['key']; ?>" class="f-control" placeholder="Search new opportunities">
                        <button type="submit" class="main-btn"><img src="<?php echo site_url() ?>/template/images/search-2.svg" alt=""></button>
                    </form>
                </div>
                <div class="open-position-wrapper load-ajax">
                    <?php 
                    $page = max(1, get_query_var('paged'));
                    $ofset = ($page - 1) * 3;
                    $args = array( 
                        'post_type' => 'job' ,
                        'posts_per_page' => 3,
                        'page' => $page,
                        'offset' => $ofset,
                        's' => ''
                    );
                    if(@$_GET['key'] != ''){
                        $args['s'] =  $_GET['key'];
                    } 
                    $mona_query = new WP_Query($args);
                    if($mona_query->have_posts())
                    {
                        while($mona_query->have_posts())
                        {
                            $mona_query->the_post();
                            get_template_part( 'patch/career/career','item' );
                        }wp_reset_query(  ) ;
                    }else
                    {
                        echo '<p>no result </p>';
                    }
                    ?>
                    
                </div>
                <div class="pagination-wrapper">
                    <?php mona_page_navi($mona_query); ?>
                    <!-- <a href="#" class="main-btn">LOAD MORE</a> -->
                </div>
            </div>
        </section>
    </main>
    <?php
endwhile;
get_footer();
?>