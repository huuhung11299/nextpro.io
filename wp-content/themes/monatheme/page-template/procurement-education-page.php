<?php
/**
 * Template name: Procurement Education page
 * @author : Hy Hý
 */
// W-1.4.2.html
get_header();
while (have_posts()):
    the_post();
    ?>
    <main class="main">
        <?php get_template_part( 'patch/section/section', 'banner' ) ?>
        <section class="procurement-section sec-bg sec">
            <div class="container">
                <div class="main-title-box text-center">
                    <h2 class="main-title"><?php the_title(); ?></h2>
                </div>
            </div>
        </section>
        
        <?php get_template_part( 'patch/section/section' , 'intro' ) ?>

        <section class="intro-section sec">
            <div class="intro-img">
                <?php echo wp_get_attachment_image( get_field('image_intro_content_edu'), '1920x400') ?>
            </div>
            <div class="intro-content">
                <div class="container">
                    <div class="main-title-box">
                        <h2 class="main-title"><?php echo str_replace('|' , '<br>', get_field('title_intro_content_edu'))  ?></h2>
                    </div>
                    <div class="text-wrapper">
                        <?php echo get_field('content_intro_content_edu') ?>
                        <br>
                        <div class="overview-wrapper">
                            <div class="columns">
                                <?php 
                                $comlun = get_field('items_overview_content_edu');
                                if(is_array($comlun))
                                {
                                    foreach($comlun as $item) 
                                    {
                                        ?>
                                <div class="column">
                                    <div class="overview-item">
                                        <div class="overview-icon">
                                            <?php echo wp_get_attachment_image( $item['image'] , '100x100' ); ?>
                                        </div>
                                        <h3 class="overview-tile"> <?php echo $item['title'] ?> </h3>
                                    </div>
                                </div>        
                                        <?php
                                    }
                                }
                                ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php //if(get_current_user_id(  ) == 1){
            ?>
        <section class="course-section sec-border-top sec">
            <div class="container">
                <div class="main-title-box text-center">
                    <h2 class="main-title">Our Public Course</h2>
                </div>
                <div class="course-wrapper">
                    <div class="columns">
                        <?php 
                        $page = max(1, get_query_var('paged'));
                        $ofset = ($page-1)*6;
                        $args = array( 
                            'post_type' => 'product',
                            'posts_per_page' => 6,
                            'page' => $page , 
                            'offset' => $ofset,
                        );
                        $course_query = new WP_Query($args);
                        while($course_query->have_posts())
                        {
                            $course_query->the_post();
                            ?>
                        <div class="column mona-course-detail">
                            <div class="course-item">
                                <div class="course-img">
                                    <a href="#course-detail-popup" data-id="<?php the_id() ?>" class="ratio-box open-popup-btn hung-detail">
                                        <?php the_post_thumbnail( '370x240' ); //   ?> 
                                    </a>
                                </div>
                                <div class="course-info">
                                    <h4 class="course-title">
                                        <a class="open-popup-btn hung-detail" data-id="<?php the_id() ?>" href="#course-detail-popup"><?php the_title() ?></a>
                                    </h4>
                                </div>
                            </div>
                        </div>      
                            <?php
                        } wp_reset_query(  );    ?> 
                    </div>
                </div> 
            </div>
        </section>    
        <!-- <a class="open-popup-btn" href="#training-registration-popup">training-registration-popup</a> </br>
        <a class="open-popup-btn" href="#draft-invoice-popup">draft-invoice-popup</a></br>
        <a class="open-popup-btn" href="#payment-method-popup">payment-method-popup</a></br>
        <a class="open-popup-btn" href="#payment-by-bank-popup">payment-by-bank-popup</a></br>
        <a class="open-popup-btn" href="#success-popup">success-popup</a></br> -->
            <?php
       // }  //  ?>
         
    </main>

    <div id="course-detail-popup" class="main-popup mfp-hide zone-loading-ajax">
         <?php // get data by ajax ?>
    </div>
    <div id="training-registration-popup" class="main-popup mfp-hide">
        <div class="main-popup-header">
            <div class="logo">
                <img src="images/logo.png" alt="">
            </div>
            <p class="title">TRAINING REGISTRATION FORM</p>
        </div>
        <div class="main-popup-body">
            <div class="training-registration-wrapper">
                <div class="f-group-wrapper">
                    <label class="f-group-title"><span>*</span>Full name</label>
                    <div class="f-group-content">
                        <input type="text" class="f-control">
                    </div>
                </div>
                <div class="f-group-wrapper f-group-gender">
                    <label class="f-group-title"><span>*</span>Gender</label>
                    <div class="f-group-content">
                        <div class="f-group">
                            <label class="custom-radio">
                                <input type="radio" name="gender" checked>
                                <span class="checkmark"></span>
                                <div class="text-box">Male</div>
                            </label>
                        </div>
                        <div class="f-group">
                            <label class="custom-radio">
                                <input type="radio" name="gender">
                                <span class="checkmark"></span>
                                <div class="text-box">Female</div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="f-group-wrapper">
                    <label class="f-group-title"><span>*</span>Your email</label>
                    <div class="f-group-content">
                        <input type="email" class="f-control">
                    </div>
                </div>
                <div class="f-group-wrapper">
                    <label class="f-group-title"><span>*</span>Your phone number</label>
                    <div class="f-group-content">
                        <input type="text" class="f-control">
                    </div>
                </div>
                <div class="f-group-wrapper f-group-gender">
                    <label class="f-group-title"><span>*</span>Registration Mode</label>
                    <div class="f-group-content">
                        <div class="f-group">
                            <label class="custom-radio">
                                <input type="radio" name="mode" checked>
                                <span class="checkmark"></span>
                                <div class="text-box">Personal</div>
                            </label>
                        </div>
                        <div class="f-group">
                            <label class="custom-radio">
                                <input type="radio" name="mode">
                                <span class="checkmark"></span>
                                <div class="text-box">Corporate</div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="f-group-wrapper f-group-quality">
                    <label class="f-group-title"><span>*</span>Quantity</label>
                    <div class="f-group-content">
                        <select name="" id="" class="f-control">
                            <option value="">02</option>
                            <option value="">02</option>
                        </select>
                        <p><strong>Total Amount:</strong> 40,000,000 vnđ</p>
                    </div>
                </div>
                <div class="f-group-wrapper f-group-participants">
                    <label class="f-group-title"><span>*</span>Information of Sub-registered Participants</label>
                    <div class="f-group-content">
                        <div class="f-group w-15">
                            <select class="f-control">
                                <option value="">Male</option>
                                <option value="">Male</option>
                            </select>
                        </div>
                        <div class="f-group w-35">
                           <input type="text" class="f-control" placeholder="Full name">
                        </div>
                        <div class="f-group w-25">
                           <input type="number" class="f-control" placeholder="Phone number">
                        </div>
                        <div class="f-group w-25">
                            <input type="email" class="f-control" placeholder="Email">
                        </div>
                        <div class="f-group w-15">
                            <select class="f-control">
                                <option value="">Male</option>
                                <option value="">Male</option>
                            </select>
                        </div>
                        <div class="f-group w-35">
                           <input type="text" class="f-control" placeholder="Full name">
                        </div>
                        <div class="f-group w-25">
                           <input type="number" class="f-control" placeholder="Phone number">
                        </div>
                        <div class="f-group w-25">
                            <input type="email" class="f-control" placeholder="Email">
                        </div>
                    </div>
                </div>
                <div class="f-group-wrapper">
                    <label class="f-group-title"><span>*</span>Your Company Name</label>
                    <div class="f-group-content">
                        <input type="text" class="f-control">
                    </div>
                </div>
                <div class="f-group-wrapper">
                    <label class="f-group-title"><span>*</span>Your Position</label>
                    <div class="f-group-content">
                        <input type="text" class="f-control">
                    </div>
                </div>
                <div class="f-group-wrapper f-group-vat">
                    <label class="f-group-title"><span>*</span>VAT Invoice</label>
                    <div class="f-group-content">
                        <select name="" id="" class="f-control">
                            <option value="">Yes</option>
                        </select>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr</p>
                    </div>
                </div>
                <div class="f-group-wrapper">
                    <label class="f-group-title"><span>*</span>Your Company Address</label>
                    <div class="f-group-content">
                        <input type="text" class="f-control">
                    </div>
                </div>
                <div class="f-group-wrapper">
                    <label class="f-group-title"><span>*</span>Your Company Tax Code</label>
                    <div class="f-group-content">
                        <input type="text" class="f-control">
                    </div>
                </div>
                <div class="f-group-wrapper">
                    <label class="f-group-title">Messages</label>
                    <div class="f-group-content">
                        <textarea class="f-control"></textarea>
                    </div>
                </div>
                <div class="f-group-wrapper f-group-txt">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr</div>
                <div class="f-group-wrapper">
                    <label class="custom-radio">
                        <input type="checkbox" name="accept">
                        <span class="checkmark"></span>
                        <div class="text-box">I agress to accept Nextpro’s <a href="#" class="hl-color"><u>Privacy Policy</u></a> and <a href="#" class="hl-color"><u>Term of Service</u></a></div>
                    </label>
                </div>
                <div class="f-group-wrapper f-group-request">
                    <div class="main-popup-btn-box">
                        <p class="title">Request Draft Invoice</p>
                        <div class="btn-box">
                            <button type="submit" class="main-btn alt">PAY LATER</button>
                            <button type="submit" class="main-btn">PAY NOW</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="draft-invoice-popup" class="main-popup mfp-hide">
        <div class="main-popup-header">
            <div class="logo">
                <img src="images/logo.png" alt="">
            </div>
            <p class="title">DRAFT INVOICE</p>
        </div>
        <div class="main-popup-body">
            <div class="draft-invoice-wrapper">
                <div class="draft-invoice-info">
                    <div class="info-item">
                        <p><strong>Bill from:</strong></p>
                        <p>Company Name: Nextpro IPS Ltd.,Co</p>
                        <p>Tax code: 123345678</p>
                        <p>Street Addres: 15 Nguyen Trung Ngan</p>
                        <p>District: 1</p>
                        <p>City: Ho Chi Minh city</p>
                        <p>Phone: +84 28 35889 123</p>
                    </div>
                    <div class="info-item">
                        <p><strong>Bill to:</strong></p>
                        <p>Company Name: Nextpro IPS Ltd.,Co</p>
                        <p>Tax code: 123345678</p>
                        <p>Street Addres: 15 Nguyen Trung Ngan</p>
                        <p>District: 1</p>
                        <p>City: Ho Chi Minh city</p>
                        <p>Phone: +84 28 35889 123</p>
                    </div>
                    <div class="info-item">
                        <p><strong>Invoice No:</strong> 123456789</p>
                        <p><strong>Invoice No:</strong> 15/03/2020</p>
                    </div>
                </div>
                <div class="draft-invoice-table">
                    <table>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Description</th>
                                <th>Quantity</th>
                                <th>Unit Price (VND)</th>
                                <th>Total (VND)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Register to Training Course NP-AT-001</td>
                                <td>02</td>
                                <td>20,000,000</td>
                                <td>40,000,000</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Register to Training Course NP-AT-001</td>
                                <td>02</td>
                                <td>20,000,000</td>
                                <td>40,000,000</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Register to Training Course NP-AT-001</td>
                                <td>02</td>
                                <td>20,000,000</td>
                                <td>40,000,000</td>
                            </tr>
                            <tr>
                                <td colspan="3" rowspan="3"></td>
                                <td><strong>Subtotal</strong></td>
                                <td><strong>40,000,000</strong></td>
                            </tr>
                            <tr>
                                <td>VAT Tax (0%)</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td><strong>Total</strong></td>
                                <td><strong>40,000,000</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="main-popup-btn-box">
                    <p class="title"></p>
                    <div class="btn-box">
                        <button type="submit" class="main-btn">EDIT</button>
                        <button type="submit" class="main-btn alt">REQUEST</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="payment-method-popup" class="main-popup mfp-hide">
        <div class="main-popup-header">
            <div class="logo">
                <img src="<?php echo site_url() ?>/template/images/logo.png" alt="">
            </div>
        </div>
        <div class="main-popup-body">
            <div class="payment-method-wrapper">
                <p class="payment-method-title">PAYMENT METHOD SELECTION</p>
                <div class="payment-method-list">
                    <div class="payment-method-item">
                        <label class="custom-radio">
                            <input type="radio" name="payment">
                            <span class="checkmark"></span>
                            <div class="text-box">
                                <p class="title">Payment by local ATM</p>
                                <ul class="visa-nav">
                                    <li><img src="<?php echo site_url() ?>/template/images/1.jpg" alt=""></li>
                                    <li><img src="<?php echo site_url() ?>/template/images/2.jpg" alt=""></li>
                                    <li><img src="<?php echo site_url() ?>/template/images/3.jpg" alt=""></li>
                                    <li><img src="<?php echo site_url() ?>/template/images/4.jpg" alt=""></li>
                                    <li><img src="<?php echo site_url() ?>/template/images/5.jpg" alt=""></li>
                                    <li><img src="<?php echo site_url() ?>/template/images/6.jpg" alt=""></li>
                                    <li><img src="<?php echo site_url() ?>/template/images/7.jpg" alt=""></li>
                                    <li><img src="<?php echo site_url() ?>/template/images/8.jpg" alt=""></li>
                                </ul>
                            </div>
                        </label>
                    </div>
                    <div class="payment-method-item">
                        <label class="custom-radio">
                            <input type="radio" name="payment">
                            <span class="checkmark"></span>
                            <div class="text-box">
                                <p class="title">Payment by <strong>local ATM</strong></p>
                            </div>
                        </label>
                    </div>
                    <div class="payment-method-item">
                        <label class="custom-radio">
                            <input type="radio" name="payment">
                            <span class="checkmark"></span>
                            <div class="text-box">
                                <p class="title">Payment by <strong>Bank Transfer</strong></p>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="main-popup-btn-box">
                    <p class="title"></p>
                    <div class="btn-box">
                        <button type="submit" class="main-btn">SUBMIT</button>
                        <button type="submit" class="main-btn alt">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="payment-by-bank-popup" class="main-popup mfp-hide">
        <div class="main-popup-header">
            <div class="logo">
                <img src="<?php echo site_url() ?>/template/images/logo.png" alt="">
            </div>
            <p class="title">PAYMENT BY BANK TRANSFER</p>
        </div>
        <div class="main-popup-body">
            <div class="training-registration-wrapper">
                <div class="f-group-wrapper f-group-amount">
                    <label class="f-group-title">Full name</label>
                    <div class="f-group-content">
                        <input type="text" class="f-control">
                    </div>
                </div>
                <div class="f-group-wrapper">
                    <label class="f-group-title">Bank information</label>
                    <div class="f-group-content">
                        <textarea class="f-control"></textarea>
                    </div>
                </div>
                <div class="f-group-wrapper f-group-reference">
                    <label class="f-group-title">Reference</label>
                    <div class="f-group-content">
                        <div class="f-group">
                            <p>Course No.</p>
                            <input type="text" class="f-control">
                        </div>
                        <div class="f-group">
                            <p>Course Name.</p>
                            <input type="text" class="f-control">
                        </div>
                    </div>
                </div>
                <div class="f-group-wrapper f-group-txt">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                    Lorem ipsum dolor sit amet, consetetur.
                </div>
                <div class="f-group-wrapper">
                    <label class="f-group-title"><span>*</span>Bank Account Number</label>
                    <div class="f-group-content">
                        <input type="text" class="f-control" placeholder="Input text">
                    </div>
                </div>
                <div class="f-group-wrapper">
                    <label class="f-group-title"><span>*</span>Bank Name & Address</label>
                    <div class="f-group-content">
                        <input type="text" class="f-control" placeholder="Input text">
                    </div>
                </div>
                <div class="f-group-wrapper f-group-calendar">
                    <label class="f-group-title"><span>*</span>Date of Payment</label>
                    <div class="f-group-content">
                        <input type="text" class="f-control date-picker" placeholder="Input text">
                    </div>
                </div>
                <div class="f-group-wrapper f-group-attachment">
                    <label class="f-group-title"><span>*</span>Payment Order Attachment</label>
                    <div class="f-group-content">
                        <input type="file" class="f-control" placeholder="Input text">
                    </div>
                </div>
                <div class="f-group-wrapper f-group-txt">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr</div>
                <div class="f-group-wrapper f-group-request">
                    <div class="main-popup-btn-box">
                        <p class="title"></p>
                        <div class="btn-box">
                            <button type="submit" class="main-btn">SUBMIT</button>
                            <button type="submit" class="main-btn alt">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="success-popup" class="main-popup mfp-hide">
        <div class="main-popup-header">
            <div class="logo">
                <img src="<?php echo site_url() ?>/template/images/logo.png" alt="">
            </div>
        </div>
        <div class="main-popup-body">
            <div class="success-wrapper">
                <p class="success-title">THANK YOU SO MUCH!</p>
                <p class="success-sub-title">Your request has been submitted successfully. Please check <a href="#" class="link">your email inbox</a><br>for more details</p>
            </div>
        </div>
    </div>
    <?php
endwhile;
get_footer();
?>