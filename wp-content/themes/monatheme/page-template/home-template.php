<?php
/**
 * Template name: Home Page
 * @author : Hy Hý
 */
get_header();
while (have_posts()):
    the_post();
    ?>
   <main class="main">
        <div class="home-banner-section sec">
            <div class="home-banner-slider-wrapper">
                <div class="home-banner-slider main-slider main-slider-stretch arrow-inside">
                    <?php $slider = get_field('items_slider_home_page');
                    if(is_array($slider))
                    {
                        foreach($slider as $item)
                        {
                            ?>
                    <div class="slide-item">
                        <div class="home-banner-slider-item" style="background-image: url(<?php echo wp_get_attachment_image_url($item['background'],'full') ?>);">
                            <div class="container">
                                <div class="home-banner-info">
                                    <p class="home-banner-title wow fadeInLeft" data-wow-delay="0.3s"><?php echo $item['title'] ?></p>
                                    <div class="home-banner-desc wow fadeInLeft" data-wow-delay="0.6s"><?php echo $item['content'] ?></div>
                                    <div class="home-banner-intro">
                                        <div class="columns">
                                            <?php $fi = $item['slider_field'];
                                            if(is_array($fi))
                                            {
                                                foreach($fi as $k => $itm)
                                                {
                                                    ?>
                                            <div class="column wow fadeInLeft" data-wow-delay="0.<?php echo $k + 6 ?>s">
                                                <div class="number"><span class="countUp-number"><?php echo $itm['number'] ?></span>+</div>
                                                <div class="title"><?php echo $itm['text'] ?></div>
                                            </div>        
                                                    <?php
                                                }
                                            } 
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                                <div class="home-banner-img wow fadeInRight" data-wow-delay="0.3s">
                                    <?php echo wp_get_attachment_image( $item['image'],'680x536' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>        
                            <?php
                        }
                    }
                    ?> 
                </div>
                <div class="home-banner-slider-nav main-slider main-slider-stretch">
                    <?php  
                    if(is_array($slider))
                    {
                        foreach($slider as $k =>  $item)
                        {
                            ?>
                    <div class="slide-item wow fadeInUp" data-wow-delay="0.<?php echo $k ?>s">
                        <p class="title"><?php echo $item['title_2'] ?></p>
                        <div class="content"> <?php echo $item['content_2'] ?></div>
                    </div>        
                            <?php
                        } 
                    }?> 
                </div>
            </div>
        </div>
        <div class="key-insights-section sec">
            <p class="key-insights-title wow fadeInLeft"><?php echo __('KEY INSIGHTS', 'monamedia') ?></p>
            <div class="key-insights-slider main-slider wow fadeInRight">
                <?php $key = get_field('items_key_insights_home');
                if(is_array($key)) 
                    foreach($key as $item) 
                        echo '<div class="slide-item"><a href="'.$item['link'].'">'.$item['title'].'</a></div>';
                ?> 
            </div>
        </div>
        <section class="home-section market-research-section sec">
            <div class="container">
                <div class="home-section-decor">
                    <p class="home-section-decor-title"><?php echo get_field('title_market_research_home'); ?></p>
                </div>
                <div class="main-title-box">
                    <h2 class="main-title-4"><?php echo get_field('title_market_research_home'); ?></h2>
                </div>
                <div class="market-research">
                    <div class="columns">
                        <div class="column market-research-left wow fadeInLeft">
                            <div class="text-wrapper">
                                <?php echo get_field('content_market_research_home'); ?>
                            </div>
                        </div>
                        <div class="column market-research-center wow fadeInUp">
                            <div class="achievement-wrapper">
                            <?php $filed =  get_field('content_field_market_research_home'); ?>
                                 <?php 
                                 if(is_array($filed))
                                 {
                                     foreach($filed as $item)
                                     {
                                        ?>
                                <div class="achievement-item">
                                    <p class="achievement-title"><?php echo $item['title'] ?></p>
                                    <p class="achievement-number"><span class="counter"><?php echo $item['content'] ?></span></p>
                                </div>       
                                        <?php
                                     }
                                 } ?>
                               
                            </div>
                        </div>
                        <div class="column market-research-right wow fadeInRight">
                            <div class="market-research-img">
                                <?php echo wp_get_attachment_image( get_field('image_market_research_home'), 'full' ) ?>
                                <?php $link_video = get_field('link_youtube_market_research_home');
                                if($link_video != '')
                                {
                                    ?>
                                <a href="#open-popup-view" data-link="<?php echo wp_get_attachment_url( $link_video ); ?>" 
                                class="show-video open-popup-btn mona-open-video-btn">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                </a>    
                                    <?php
                                }
                                ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="home-section-direct wow fadeInRight">
                    <p class="home-section-direct-title"><?php echo get_field('title_2_market_research_home') ?></p>
                    <?php $link = get_field('direct_link_market_research_home');
                    if(is_array($link))
                    {
                        foreach($link as $itm)
                        {
                            echo '<a href="'.$itm['link'].'" target="_blank"  class="home-section-direct-link">'.$itm['title'].'</a>';
                        }
                    }
                    ?>
                    
                </div>
            </div>
        </section>
        <section class="home-section request-for-quotes-section sec">
            <div class="container">
                <div class="home-section-decor fadeInRight wow">
                    <p class="home-section-decor-title"><?php echo get_field('title_request_for_quotes') ?></p>
                </div>
                <div class="request-for-quotes">
                    <div class="columns">
                        <div class="column request-for-quotes-left fadeInLeft wow">
                            <div class="request-for-quotes-img fadeInLeft wow">
                                <?php echo wp_get_attachment_image( get_field('image_request_for_quotes'),'full' ) ?>
                                <?php $link_video = get_field('link_youtube_request_for_quotes') ;
                                if($link_video != '')
                                {
                                    ?>
                                <a href="#open-popup-view" data-link="<?php echo wp_get_attachment_url(  $link_video )  ?>" 
                                class="show-video open-popup-btn mona-open-video-btn">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                </a>    
                                    <?php
                                }
                                ?>
                                
                            </div>
                            
                        </div>
                        <div class="column request-for-quotes-right">
                            <div class="main-title-box fadeInRight wow">
                                <h2 class="main-title-4"><?php echo get_field('title_request_for_quotes') ?></h2>
                            </div>
                            <div class="text-wrapper fadeInUp wow">
                                <?php echo get_field('content_request_for_quotes') ?>
                                <p><a href="<?php echo get_field('link_request_for_quotes') ?>" target="_blank" class="learn-more-btn"><?php echo __('LET TRY','monamedia') ?></a></p>
                            </div>
                            <div class="achievement-wrapper">
                                <?php $field = get_field('content_field_request_for_quotes');
                                if(is_array($field))
                                {
                                    foreach($field as $k =>  $item)
                                    {
                                        ?>
                                <div class="achievement-item fadeInRight wow" data-wow-delay="0.<?php echo $k * 2 ?>"> 
                                    <p class="achievement-title"><?php echo $item['title'] ?></p>
                                    <p class="achievement-number"><?php echo $item['content'] ?></p>
                                </div>        
                                        <?php
                                    }
                                }
                                ?>
                                 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="home-section-direct wow fadeInRight">
                    <p class="home-section-direct-title"><?php echo get_field('title_2_request_for_quotes') ?></p>
                    <?php $link = get_field('link_direct_request_for_quotes');
                    if(is_array($link))
                    {
                        foreach($link as $item)
                        {
                            echo '<a href="'.$item['link'].'" target="_blank" class="home-section-direct-link">'.$item['title'].'</a>';
                        }
                    } ?>
                    
                </div>
            </div>
        </section>
        <section class="home-section e-auction-section sec" 
        style="background-image: url(<?php echo wp_get_attachment_image_url( get_field('background_e_auction'), 'full' ) ?>);">
            <div class="container">
                <div class="home-section-decor fadeInLeft wow">
                    <p class="home-section-decor-title"><?php echo get_field('title_e_auction') ?></p>
                </div>
                <div class="main-title-box fadeInLeft wow">
                    <h2 class="main-title-4"><?php echo get_field('title_e_auction') ?></h2>
                </div>
                <div class="e-auction">
                    <div class="columns">
                        <div class="column e-auction-left fadeInLeft wow">
                            <div class="text-wrapper">
                                <?php echo get_field('content_e_auction') ?>
                                <p><a href="<?php echo get_field('link_e_auction') ?>" target="_black" class="learn-more-btn"><?php echo __("LET TRY",'monamedia') ?></a></p>
                            </div>
                        </div>
                        <div class="column e-auction-right">
                            <div class="achievement-wrapper">
                                <div class="columns">
                                     <?php $field = get_field('content_field_e_auction');
                                     if(is_array($field))
                                     {
                                         foreach($field as $key => $item)
                                         {
                                             ?>
                                    <div class="column fadeInRight wow" data-wow-delay="0.<?php echo $key * 2 ?>">
                                        <div class="achievement-item">
                                            <p class="achievement-title"><?php echo $item['title'] ?></p>
                                            <p class="achievement-number"><?php echo $item['number'] ?></p>
                                        </div>
                                    </div>         
                                             <?php
                                         }
                                     }
                                     ?> 
                                </div>
                            </div>
                            <?php $link_video = get_field('link_youtube_e_auction');
                            if($link_video != '')
                            {
                                ?>
                            <a  href="#open-popup-view" data-link="<?php echo wp_get_attachment_url( $link_video ) ?>" 
                            class="show-video open-popup-btn mona-open-video-btn">
                                <i class="fa fa-play" aria-hidden="true"></i>
                            </a>     
                                <?php
                            }
                            ?>
                            
                        </div>
                    </div>
                </div>
                <div class="home-section-direct fadeInRight wow">
                    <p class="home-section-direct-title"><?php echo get_field('title_2_e_auction') ?></p>
                    <?php $link = get_field('direct_link_e_auction');
                    if(is_array($link))
                    {
                        foreach($link as $item)
                        {
                            echo '<a href="'.$item['link'].'" target="_blank" class="home-section-direct-link">'.$item['title'].'</a>';
                        }
                    } ?>
                    
                </div>
            </div>
        </section>
        <section class="testimonials-section sec">
            <div class="container">
                <div class="main-title-box fadeInLeft wow">
                    <h2 class="main-title"><?php echo __('Testimonials','monamedia') ?></h2>
                </div>
            </div>
            <div class="container-plus"> 
                <div class="testimonials-slider-wrapper">
                    <div class="testimonials-slider main-slider">
                        <?php $items = get_field('testimonials_items_home_page');
                        if(is_array($items))
                        {
                            foreach($items as $item)
                            {
                                ?>
                        <div class="slide-item fadeInUp wow">
                            <div class="testimonials-item">
                                <div class="testimonials-img">
                                    <a href="#open-popup-view"   data-link="<?php echo wp_get_attachment_url( $item['link'] )   ?>" 
                                    class="show-video ratio-box open-popup-btn">
                                       <?php echo wp_get_attachment_image( $item['image'] , '470x270' ) ?>
                                    </a>
                                </div>
                                <div class="testimonials-content">
                                    <?php echo $item['content'] ?>
                                </div>
                                <div class="testimonials-author">
                                    <div class="avatar">
                                        <?php echo wp_get_attachment_image( $item['image_author'] ,'100x100' ); ?>
                                    </div>
                                    <div class="info">
                                        <p class="name"><?php echo $item['name'] ?></p>
                                        <div><?php echo __('Position','monamedia') ?>: <?php echo $item['position'] ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>         
                                <?php
                            }
                        }
                        ?> 
                    </div> 
                </div>
            </div>
            <div class="container">
                <div class="slider-append-wrapper">
                    <div class="slider-append"></div>
                </div>
            </div>
        </section>
    </main>
    <div id="open-popup-view" class="mfp-hide main-popup mona-loading-video">
        
            <video controls>
                <source src="" id="source-video" type="video/mp4">
            </video>
        
    </div>  
    <?php
endwhile;
get_footer();
?>
 
 <script>
jQuery(document).ready(function($){    
    $(".show-video").on('click',function(){
        let $link = $(this).attr('data-link');
        console.log($link)
        $('#source-video').attr('src', $link);
    });
});
    
 </script>