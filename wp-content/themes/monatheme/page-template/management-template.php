<?php
/**
 * Template name: Management Page
 * @author : Hy Hý
 */
//  W-1.1.1.html , W-1.1.2.html , W-1.1.3.html , W-1.1.4.html , W-1.2.1.html ,  W-1.2.2.html ,W-1.3.1.html 
get_header();
while (have_posts()):
    the_post();
    ?>
    <main class="main">
        <?php get_template_part( 'patch/section/section', 'banner' ) ?>
        <section class="procurement-section sec">
            <div class="container">
                <div class="main-title-box text-center">
                    <h1 class="main-title"><?php the_title(); ?></h1>
                </div>
                <div class="text-wrapper text-center">
                    <?php the_content(  ) ?>
                </div>
            </div>
        </section>

        <?php get_template_part('patch/section/section','intro' ) ?>
       
    </main>
    <?php
endwhile;
get_footer();
?>