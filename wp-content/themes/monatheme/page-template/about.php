<?php
/**
 * Template name: About Page
 * @author : Hy Hý
 */
// W-3.html#
get_header();
while (have_posts()):
    the_post();
    ?>
    <main class="main">
        <div class="banner-section sec" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field('banner_background_page') ,  '1920x590'); ?>);">
            <div class="container">
                <h1 class="banner-title mt-auto"><?php the_title() ?></h1>
            </div>
        </div>
        <?php 
        $slider = get_field('items_slider_menu');
        if(is_array($slider))
        {
        ?>
        <div class="tab-wrapper">
            <div class="about-direct-section sec">
                <div class="container">
                    <div class="about-direct-slider-nav main-slider main-slider-stretch arrow-inside">
                        <?php  
                        $current = 'current';
                        foreach($slider as $key =>  $item)
                        {
                                ?>
                        <div class="slide-item tab-link <?php echo $current ?>" 
                        data-tab="#tab-about-<?php echo $key + 1 ?>">
                            <a href="#" class="about-direct-link">
                                <span class="title"><?php echo $item['title_slider_menu'] ?></span>
                            </a>
                        </div>
                                <?php
                            $current = '';
                        } 
                        ?> 
                    </div>
                </div>
            </div> 
        
            <div class="about-section sec">
                <div class="container">
                    <div class="tab-content-wrapper">
                        <?php 
                        foreach($slider as $key =>  $items)
                        {
                        ?>
                        <div class="tab-content" id="tab-about-<?php echo $key + 1 ?>">
                            <div class="text-wrapper text-center mona-content">
                                 <?php echo $items['content'] ?> 
                            </div>
                            <?php 
                            $column = $items['items'];
                            if(is_array($column))
                            {  
                            ?>
                            <div class="about-wrapper">
                                <div class="columns">

                                    <?php  
                                    foreach($column as $item)
                                        {
                                            ?>
                                    <div class="column">
                                        <div class="about-item">
                                            <div class="about-img">
                                                <a href="<?php echo $item['link'] ?>" class="ratio-box">
                                                    <?php echo wp_get_attachment_image($item['image'], '370x230') ?>
                                                </a>
                                            </div>
                                            <div class="about-info">
                                                <p class="about-title">
                                                    <a href="<?php echo $item['link'] ?>">
                                                        <?php echo $item['title'] ?>
                                                    </a>
                                                </p>
                                                <div class="about-desc"><?php echo $item['description'] ?></div>
                                            </div>
                                        </div>
                                    </div>         
                                            <?php
                                    } 
                                    ?> 
                                </div>
                            </div>
                                <?php 
                            } ?>
                        </div>
                            <?php 
                        }
                            ?> 
                    </div>
                </div>
            </div>
        </div>
        <?php 
        } ?>
    </main>
    <?php
endwhile;
get_footer();
?>