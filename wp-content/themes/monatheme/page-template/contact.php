<?php
/**
 * Template name: Contact Page
 * @author : Hy Hý
 */
// W-7.html
get_header();
while (have_posts()):
    the_post();
    ?>
    <main class="main">
        <!-- <div class="map-banner-section sec">
            <?php //echo get_field('iframe_map_page'); /// feedback ?>
        </div> -->
        <div class="banner-section sec" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field('banner_background_page') ,  '1920x590'); ?>);">
            <div class="container">
                <h1 class="banner-title" style="margin:0"><?php the_title() ?></h1>
                <div class="banner-sub-title"><?php echo get_field('content_quote_single_post') ?></div>

            </div>
        </div>
        <section class="contact-section sec">
            <div class="container">
                <div class="main-title-box">
                    <h2 class="main-title-2"><?php //the_title(  ) ?></h2>
                </div>
                <div class="contact-wrapper">
                    <div class="columns">
                        <div class="column contact-left">
                            <div class="contact-info-wrapper">
                                <?php 
                                $contact_field = get_field('items_field_contact');
                                if(is_array($contact_field))
                                {
                                    foreach($contact_field as $item)
                                    {
                                        ?>
                                        
                                <div class="contact-info-item">
                                    <p class="title"><?php echo $item['title'] ?></p>
                                    <div class="text-wrapper mona-content">
                                        <?php echo $item['content']; ?>
                                    </div>
                                </div>        <?php
                                    }
                                }
                                ?>
                                
                            </div>
                        </div>
                        <div class="column contact-right">
                            <div class="contact-form-wrapper">
                                <p class="title"><?php echo get_field('title_form_contact_page') ?></p>
                                <div class="text-wrapper"><?php echo get_field('intro_form_contact') ?></div>
                                <div class="contact-form">
                                    <?php $short_code = get_field('shortcode_contact_form_page');
                                    
                                    if($short_code != '') echo do_shortcode($short_code);    
                                      
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
endwhile;
get_footer();
?>