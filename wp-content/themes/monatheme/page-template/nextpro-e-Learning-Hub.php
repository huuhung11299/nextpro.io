<?php
/**
 * Template name: nextpro e-Learning Hub
 * @author : Hy Hý
 */ 
//  W-1.4.1.html
get_header();
while (have_posts()):
    the_post();
    ?>
    <main class="main">
        <div class="banner-section sec" style="background-image: url(<?php  echo wp_get_attachment_image_url( get_field('background_banner_management') , '1920x590' ) ?>);">
            <div class="container">
                <h2 class="banner-title"><?php echo get_field('title_banner_managementIma') ?></h2>
                <h3 class="mb-auto"><?php echo get_field('subtitle_banner_managementIma') ?></h3>
                <div class="banner-sub-title"><?php echo get_field('content_banner_managementIma') ?></div>
            </div>
        </div>
        <section class="procurement-section sec-bg sec">
            <div class="container">
                <div class="main-title-box text-center">
                    <h1 class="main-title"><?php the_title() ?></h1>
                </div>
            </div>
        </section>
        <section class="overview-section sec">
            <div class="container">
                <div class="columns">
                    <div class="column overview-section-left">
                        <div class="main-title-box">
                            <h2 class="main-title"><?php echo get_field('title_overview'); ?></h2>
                        </div>
                        <div class="text-wrapper">
                            <?php echo get_field('content_overview') ?>
                        </div>
                        <div class="overview-wrapper">
                            <div class="columns">
                                <?php 
                                $column = get_field('items_overview');
                                if(is_array($column))
                                {
                                    foreach($column as $item){    ?>

                                <div class="column">
                                    <div class="overview-item">
                                        <div class="overview-icon">
                                            <?php echo wp_get_attachment_image($item['image'], '100x100' ) ?>
                                        </div>
                                        <h3 class="overview-tile"><?php echo $item['content']; ?></h3>
                                    </div>
                                </div>       
                                       <?php
                                    }
                                }    ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="column overview-section-right">
                        <?php echo wp_get_attachment_image( get_field('image_overview') , '680x536' )  ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="intro-section sec">
            <div class="intro-img">
                <?php echo wp_get_attachment_image( get_field('image_intro_hub'), '1920x400' ) ?>
            </div>
            <div class="intro-content-2">
                <div class="container">
                    <div class="text-wrapper">
                        <?php echo get_field('content_intro_hub'); ?>
                    </div>
                </div>
            </div>
        </section>
        <div class="start-your-journey-section">
            <div class="container">
                <a href="<?php echo get_field('link_button_intro_hub') ?>" target="_black" class="learn-more-btn"><?php echo get_field('title_button_intro_hub') ?></a>
            </div>
        </div>
    </main>
    <?php
endwhile;
get_footer();
?>