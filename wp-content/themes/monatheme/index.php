<?php
get_header();
?>
    <main class="main">
        <div class="banner-section sec" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field('banner_blog_page', MONA_BLOG),'full' ) ?>);">
            <div class="container">
                <h1 class="banner-title mt-auto"><?php echo get_the_title(MONA_BLOG) ?></h1>
                <?php $postId= get_field('choose_post_blog_page',MONA_BLOG);
                if($postId != '')
                {
                    ?>
                <div class="banner-bottom">
                    <p class="banner-title-2">
                        <a href="<?php echo get_permalink( $postId ) ?>">
                            <?php echo get_the_title( $postId ) ?>
                        </a>
                    </p>
                    <p> <?php echo get_the_time( 'M d, Y' ) ?> <?php echo __('By','monamedia') ?> 
                        <a href="<?php echo get_author_posts_url(get_post_field( 'post_author', $postId )) ?>">
                            <strong>
                                <?php echo get_the_author_meta( 'nickname', get_post_field( 'post_author', $postId ) ) ?>
                            </strong>
                        </a>
                    </p>
                </div>
                    <?php
                }
                ?>
                
            </div>
        </div>
        <section class="news-section sec">
            <div class="container">
                <div class="news-wrapper">
                    <?php while(have_posts())
                    {
                        the_post();
                        get_template_part( 'patch/post/post', 'item' );
                    }
                    wp_reset_query(  ); ?>
                   
                </div>
                <div class="pagination-wrapper">
                  <?php mona_page_navi(); ?>
                     
                </div>
            </div>
        </section>
    </main>
<?php get_footer();
