<?php
if (is_user_logged_in()) {
    wp_redirect(get_home_url());
}
get_header();
while (have_posts()):
    the_post(); 
    ?> 
    <main id="main">
        <div class="login-section sec">
            <div class="container">
                <div class="login-form-wrapper wow fadeInDown">
                    <h1 class="md-title login-title"><?php the_title() ?></h1>
                    <p class="login-desc">Nhập email của bạn nhấn gửi bạn sẽ nhận được 1 mail chứa link xác nhận</p>
                    <?php
                if (!isset($_GET['reset'])) {
                    ?> 
                    <form action="" method="post" id="mona-foget-submit-form"  class="login-form">
                        <input type="hidden" name="key" value="<?php echo $_GET['key']; ?>"/>
                        <input type="hidden" name="login" value="<?php echo $_GET['login']; ?>"/>
                        <div class="f-group"> 
                            <input type="email" name="register_email" class="f-control" required placeholder="Email của bạn">
                        </div> 
                        <div class="f-group">
                            <input type="hidden" id="data-redierect" value="<?php echo esc_url(@$_GET['redirect_to'])?>"/> 
                            <button type="submit" class="submit-btn mona-has-ajax mona-fix-btn-width">Lấy lại mật khẩu</button>
                            <p class="foget-pass-link">Bạn có tài khoản? <a href="#" class="mona-action-login hl-txt"><?php _e('đăng nhập ngay', 'monamedia'); ?></a></p>
                        </div>
                        <p id="response-foget" class="response-foget red"></p>
                    </form>
                <?php }else{
                    ?>
                    <form action="" method="post" id="mona-foget-submit-form"  class="login-form">
                        <input type="hidden" name="key" value="<?php echo $_GET['key']; ?>"/>
                        <input type="hidden" name="login" value="<?php echo $_GET['login']; ?>"/>
                        <div class="f-group"> 
                            <input required id="your-pass" name="new_password"  type="password" class="fcontrol" placeholder="Mật khẩu mới"> 
                        </div> 
                        <div class="f-group">  
                            <input required id="verify-pass" name="usr_reset_pass"  type="password" class="fcontrol" placeholder="Xác nhận mật khẩu">
                        </div> 
                        <div class="f-group"> 
                            <button type="submit" class="submit-btn mona-has-ajax mona-fix-btn-width">Lưu mật khẩu</button> 
                        </div>
                        <p id="response-foget" class=" red"></p>
                    </form>
                    <?php
                } ?>
                </div>
            </div>
        </div>
    </main>
    <?php
endwhile;
get_footer();
?>